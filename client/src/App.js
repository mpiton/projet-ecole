import React from 'react';
import Login from './pages/auth/Login';
import Dashboard from './pages/admin/Dashboard'
import {Switch, Route} from 'react-router-dom'
import "./index.css";
import Authenticated from './components/actions/Authenticated';
import "bootstrap/dist/css/bootstrap.min.css";
import AddProf from './pages/admin/profs/AddProf';
import ListeProfs from './pages/admin/profs/ListeProfs';
import ModalAddProf from './pages/admin/profs/ModalAddProf';
import ListeEleves from './pages/admin/eleves/ListeEleves'
import Reunions from './pages/admin/reunion/Reunions'
import { Container } from '@material-ui/core';
import Logout from './components/actions/Logout';
import EmploiDuTps from './pages/admin/eleves/EmploiDuTps'
import GestionClasses from './pages/admin/eleves/GestionClasses'
import AddEleve from './pages/admin/eleves/AddEleve';
import GestionMatieres from './pages/admin/profs/GestionMatiere';
import ProfDetail from '../src/pages/admin/profs/ProfDetail'
import Note from '../src/pages/admin/eleves/Note'
import Stats from "./pages/admin/Stats"

function App() {
   return (
      <div className="app" >
         <Switch>
            {/* Racine du site si authentifié */}
            <Route exact path ="/">
               <Authenticated>
                  <Dashboard/>
                  <Container>
                     <Stats/>
                  </Container>
               </Authenticated>
            </Route>

            {/*Login page si non authentifié*/}
            <Route exact path ="/login">
               <Authenticated nonAuthenticated={true}>
                  <Login/>
               </Authenticated>
            </Route>

            {/*Liste des professeurs*/}
            <Route exact path ="/liste-professeurs">
               <Authenticated>
                  <Dashboard/>
                  <Container>
                     <ListeProfs/>
                  </Container>
               </Authenticated>
            </Route>

            {/*Editer un professeur*/}
            <Route exact path ="/professeur/:id" component={ProfDetail}>
               <Authenticated>
                  <Dashboard/>
                  <Container>
                     <ProfDetail/>
                  </Container>
               </Authenticated>
            </Route>

            {/*Component réunion*/}
            <Route exact path ="/reunions">
               <Authenticated>
                  <Dashboard/>
                  <Container>
                     <Reunions/>
                  </Container>
               </Authenticated>
            </Route>

            {/*Ajout d'un professeur en modal*/}
            <Route exact path ="/add-professeurs">
               <Authenticated>
                  <Dashboard/>
                  <Container>
                     <ModalAddProf/>
                  </Container>
               </Authenticated>
            </Route>

            {/*Liste des élèves*/}
            <Route exact path ="/liste-eleves">
               <Authenticated>
                  <Dashboard/>
                  <Container>
                     <ListeEleves/>
                  </Container>
               </Authenticated>
            </Route>

            {/*Gestion des notes des élèves*/}
            <Route exact path ="/notes">
               <Authenticated>
                  <Dashboard/>
                  <Container>
                     <Note/>
                  </Container>
               </Authenticated>
            </Route>

            {/*Gestion des classes*/}
            <Route exact path ="/add-classe">
               <Authenticated>
                  <Dashboard/>
                  <Container>
                     <GestionClasses/>
                  </Container>
               </Authenticated>
            </Route>

            {/*Gestion des matières*/}
            <Route exact path ="/add-matiere">
               <Authenticated>
                  <Dashboard/>
                  <Container>
                     <GestionMatieres/>
                  </Container>
               </Authenticated>
            </Route>

            {/*Ajouter un élève en modal*/}
            <Route exact path ="/add-eleve">
               <Authenticated>
                  <Dashboard/>
                  <Container>
                     <AddEleve/>
                  </Container>
               </Authenticated>
            </Route>

            {/*Emploi du temps des classes*/}
            <Route exact path ="/emploidutemps">
               <Authenticated>
                  <Dashboard/>
                  <Container>
                     <EmploiDuTps/>
                  </Container>
               </Authenticated>
            </Route>

            {/*Paramètres du site*/}
            <Route exact path ="/settings">
               <Authenticated>
                  <Dashboard/>
                  <Container>
                     <AddProf/>
                  </Container>
               </Authenticated>
            </Route>

            {/*Déconnexion du panel admin*/}
            <Route exact path ="/logout">
               <Logout/>
            </Route>

            {/*Reste des routes*/}
            <Route path ="*" render = {()=>"404 not found!"} />
         </Switch>
      </div>
   );
}

export default App;

