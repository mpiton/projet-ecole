import React, { Component } from 'react'
import {Container, Box, Typography, TextField, CircularProgress, Button} from '@material-ui/core'
import logo from "../../images/logo.png"
import {firebaseAuth, firestore} from "../../firebase";



export class Login extends Component {

   constructor(props) {
      super(props)

      this.state = { //Valeur par défaut
         email:"",
         password:"",
         show_progress:false,
         email_error: null,
         password_error: null
      }
      this.handleChange = this.handleChange.bind()
      this.login = this.login.bind()
   }

   componentDidMount() {
      this.setState({
         email:"",
         password:"",
         show_progress:false,
         email_error: null,
         password_error: null
      })
   }

   //Récupére les infos des champs remplit par l'utilisateur
   handleChange = (e) => this.setState({
      [e.target.name]:e.target.value
   })

   //Method d'authentification avec firebase
   login = () => {
      let valid_data = true;

      if (this.state.email==="") { //Si champs email vide
         this.setState({email_error: "Email Obligatoire!"})
         valid_data = false
         //console.log(this.state.email_error);
      }
      if (this.state.password==="") { //Si mot de passe vide
         this.setState({password_error: "Mot de passe Obligatoire!"})
         valid_data = false
      }

      if (valid_data) {
         this.setState({show_progress: true})
      }

      this.setState({
         update:true
      })

      if (valid_data) { //Si un champ rempli
         //Récupération des data sur Firestore et comparaison
         firestore.collection('USERS')
         .where('email', "==", this.state.email)
         .where('isAdmin', "==", true)
         .get()
         .then(querySnapshot => {
            if (!querySnapshot.empty) {
               //login ok
               firebaseAuth.signInWithEmailAndPassword(
                  this.state.email,
                  this.state.password
               ).then(res => {
                  this.props.history.replace('/');
               }).catch((err) =>{
                   //Si code erreur renvoyé par firebase est =
                  if (err.code === 'auth/wrong-password') {
                     this.setState({password_error: "Mot de passe incorrect!"})
                  }
                  this.setState({
                     show_progress:false
                  })
               })
            } else {
               this.setState({
                  email_error: "Accès interdit!",
                  show_progress:false
               })
            }
         })
      }
   }

   render() {
      return (
         <Container maxWidth="xs" >
            <Box
               boxShadow="1"
               borderRadius="3px"
               textAlign="center"
               p="24px"
               mt="50px"
               style={{backgroundColor: 'white'}}
            >
               <img src={logo} width="50px" height="50px" alt="logo de l'école"/>
               <Typography variant="h5" color="textSecondary">ADMIN</Typography>
               <TextField
               label="Email"
               id="outlined-size-small"
               variant="outlined"
               type="email"
               color="secondary"
               size="small"
               fullWidth
               name="email"
               onChange={this.handleChange}
               error={this.state.email_error!= null}
               helperText={this.state.email_error}
               margin="normal"/>
               <TextField
               label="Mot de passe"
               id="outlined-size-small"
               type="password"
               onChange={this.handleChange}
               error={this.state.password_error!=null}
               helperText={this.state.password_error}
               variant="outlined"
               size="small"
               fullWidth
               name="password"
               color="secondary"
               margin="normal"
               />
               {/* Bar de progression  */}
               {this.state.show_progress ? (
               <CircularProgress size={24} thickness={4} color="secondary"/>
               ) : null}
               <Button disableElevation variant="contained" color="primary" fullWidth onClick={this.login}>Valider</Button>
            </Box>
         </Container>
      )
   }
}

export default Login
