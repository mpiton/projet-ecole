import { Box, Typography } from '@material-ui/core';
import React from 'react';
import { VictoryChart, VictoryLine, VictoryZoomContainer } from "victory";

class Presence extends React.Component {

   constructor() {
     super();
     this.state = {};
   }
 
   handleZoom(domain) {
     this.setState({selectedDomain: domain});
   }
 
   handleBrush(domain) {
     this.setState({zoomDomain: domain});
   }
 
   render() {
     return (
          <React.Fragment>
            <Box 
               minWidth="45%"
               boxShadow="1" 
               borderRadius="3px"
               p="24px"
               mt="50px"
               style={{backgroundColor: 'white'}}
            >
               <Typography variant="h5" color="textSecondary">Présentiel des élèves</Typography>
           <VictoryChart
             width={550}
             height={300}
             scale={{x: "time"}}
             containerComponent={
               <VictoryZoomContainer responsive={false}
                 zoomDimension="x"
                 zoomDomain={this.state.zoomDomain}
                 onZoomDomainChange={this.handleZoom.bind(this)}
               />
             }
           >
             <VictoryLine
               style={{
                 data: {stroke: "tomato"}
               }}
               data={[
                 {x: new Date(2020, 1, 9), y: 460},
                 {x: new Date(2020, 1, 10), y: 480},
                 {x: new Date(2020, 1, 11), y: 440},
                 {x: new Date(2020, 1, 12), y: 512},
                 {x: new Date(2020, 1, 13), y: 500},
                 {x: new Date(2020, 1, 14), y: 410},
                 {x: new Date(2020, 1, 15), y: 0},
                 {x: new Date(2020, 1, 16), y: 0}
               ]}
             />
           </VictoryChart>
       
            </Box >
            </React.Fragment>
     );
   }
 }

 export default Presence