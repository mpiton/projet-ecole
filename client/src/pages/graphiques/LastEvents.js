import { Avatar, Box, Divider, List, ListItem, ListItemAvatar, ListItemText, makeStyles, Typography } from '@material-ui/core'
import React from 'react'

const useStyles = makeStyles(theme => ({
   root: {
      width: '600px',
      paddingTop: "24px",
      backgroundColor: theme.palette.background.paper,
               //smartphone
               "@media only screen and (min-device-width : 280px) and (max-device-width : 596px)": {
                  paddingTop: "10px",
                  width: "30em",
                  margin: "0",
                  overflow: "hidden"
                 },
           //smartphone
           "@media only screen and (min-device-width : 596px) and (max-device-width : 1322px)": {
            maxWidth: "39em",
            margin: "0"
           },
    },
    inline: {
      display: 'inline',
    },
 }));

export default function LastEvents() {
   const classes = useStyles();
   return (
      <Box
               minWidth="45%"
               boxShadow="1"
               borderRadius="3px"
               textAlign="center"
               mt="50px"
               style={{backgroundColor: 'white'}}
            >
         <Typography variant="h5" color="textSecondary" style={{paddingTop: "24px"}}>Derniers évènements</Typography>
         <List className={classes.root}>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar  />
        </ListItemAvatar>
        <ListItemText
          primary={new Date().toLocaleDateString("fr-FR")  + " à " + new Date().toJSON().substring(10,16).replace('T',' ')}
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                John Doe
              </Typography>
              {" à ajouté une réunion"}
            </React.Fragment>
          }
        />
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar  />
        </ListItemAvatar>
        <ListItemText
          primary={new Date().toLocaleDateString("fr-FR") + " à " + new Date().toJSON().substring(10,16).replace('T',' ')}
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                John Doe
              </Typography>
              {" à ajouté un professeur"}
            </React.Fragment>
          }
        />
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar />
        </ListItemAvatar>
        <ListItemText
          primary={new Date().toLocaleDateString("fr-FR") + " à " + new Date().toJSON().substring(10,16).replace('T',' ')}
          secondary={
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                John Doe
              </Typography>
              {' à supprimé une matière'}
            </React.Fragment>
          }
        />
      </ListItem>
    </List>
      </Box>
   )
}
