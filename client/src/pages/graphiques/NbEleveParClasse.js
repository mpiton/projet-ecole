import { Box, Typography } from '@material-ui/core';
import React from 'react';
import { VictoryPie} from "victory";
import "./graphiques.css"

class Presence extends React.Component {

   constructor() {
     super();
     this.state = {};
   }
 
   handleZoom(domain) {
     this.setState({selectedDomain: domain});
   }
 
   handleBrush(domain) {
     this.setState({zoomDomain: domain});
   }
 
   render() {
     return (
          <React.Fragment>
            <Box 
               minWidth="45%"
               boxShadow="1"
               borderRadius="3px"
               textAlign="center"
               p="24px"
               mt="50px"
               style={{backgroundColor: 'white'}}
            >
               <Typography variant="h5" color="textSecondary">Élèves par classe</Typography>
               <VictoryPie
               width={500}
  colorScale={["tomato", "orange", "gold", "cyan", "navy" ]}
  data={[
   { x: "6ème A", y: 8.33 },
   { x: "6ème B", y: 8.33 },
   { x: "6ème C", y: 8.33 },
   { x: "5ème A", y: 8.33 },
   { x: "5ème B", y: 8.33 },
   { x: "5ème C", y: 8.33 },
   { x: "4ème A", y: 8.33 },
   { x: "4ème B", y: 8.33 },
   { x: "4ème C", y: 8.33 },
   { x: "3ème A", y: 8.33 },
   { x: "3ème B", y: 8.33 },
   { x: "3ème C", y: 8.33 },

 ]}
/>
            </Box >
            </React.Fragment>
     );
   }
 }

 export default Presence