import React, { useState, useEffect } from "react";
import RoutesProfs from "../../../components/routes/RoutesProfs";
import RoutesMatieres from "../../../components/routes/RoutesMatieres";
import '../../../css/ProfListe.css'
import ProfTablo from "./ProfTablo"
import Pagination from "./Pagination"
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
   search: {
      display: 'flex',
    flexWrap: "wrap",
    marginRight: "-15px",
    marginLeft: "-15px",
    maxWidth: "750px",
     [theme.breakpoints.down("xs")]: {
       display: 'none',
       maxWidth: "100%"
     },
     [theme.breakpoints.between("sm", "md")]: {
      display: 'none',
      maxWidth: "100%"
     },
     "@media (max-width: 1730px)": {
      display: 'none',
       maxWidth: "100%"
     }
   },
 }));


const ListeProfs = () => {
   const [profs, setProfs] = useState([]);
   const [searchNom, setSearchNom] = useState("");
   const [currentPage, setCurrentPage] = useState(1);
   const [profParPage] = useState(10)
   const [totalProfs, setTotalProfs] = useState("")
   const [message, setMessage] = useState("")
   const [matieres, setMatieres] = useState([])

  const classes = useStyles();


  useEffect(() => {
   retrieveMatieres()
   retrieveProf();
 }, []);

  //Récupération de la valeur dans la barre search
  const onChangeSearchNom = e => {
    const searchNom = e.target.value;
    setSearchNom(searchNom);
  };

   //Modifie la valeur de la pagination
   const handleChange = (event, value) => {
      setCurrentPage(value);
   };

   //Récupération de tous les professeurs
   const retrieveProf = () => {
      RoutesProfs.getAll()
      .then(response => {
         setProfs(response.data.profs);
         setTotalProfs(response.data.total)
      })
      .catch(e => {
         console.log(e);
      });
   };

   //Récupération de toutes les matières
   const retrieveMatieres = () => {
      RoutesMatieres.getAll()
      .then(response => {
         setMatieres(response.data.matieres);
         // console.log(response.data.matieres);
      })
      .catch(e => {
         console.log(e);
      });
   };

   //Recherche par nom
   const findByNom= () => {
   RoutesProfs.findByNom(searchNom)
      .then(response => {
      setProfs(response.data.prof);
      setMessage()
      })
      .catch(e => {
         console.log(e);
         if (e.response.status === 404) {
            return setMessage(e.response.data.message)
         }
      });
};

//Récupération des profs actuellement affichés
const indexOfLastProf = currentPage * profParPage;
const indexOfFirstProf = indexOfLastProf - profParPage;
const currentProf = profs.slice(indexOfFirstProf, indexOfLastProf)


return (
   <div>
      <div className={classes.search}>
         <div className="col-md-8">
            <div className="input-group mb-3">
               <input
               type="text"
               className="form-control"
               placeholder="Rechercher par nom"
               value={searchNom}
               onChange={onChangeSearchNom}
               />
            <div className="input-group-append">
            <button
            className="btn btn-outline-secondary"
            type="button"
            onClick={findByNom}
            >
            Rechercher
            </button>
            </div>
         </div>
      </div>
   </div>
   {message ? (
      <h3>{message}</h3>
   ): (
   <React.Fragment>
   <ProfTablo matieres={matieres} retrieveProf={retrieveProf} professeurs={currentProf} />
   <Pagination change={handleChange} profParPage={profParPage} totalProfs={totalProfs} />
   </React.Fragment>
   )}

   </div>
);
};


export default ListeProfs;
