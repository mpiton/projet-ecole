import React, { useState } from "react";
import RoutesMatieres from "../../../components/routes/RoutesMatieres";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import {
   Input, 
   FormControl,
   InputLabel,
   Button,
   FormHelperText,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

//Message sucess
function Alert(props) {
   return <MuiAlert elevation={6} variant="filled" {...props} />;
 }

//Style
const useStyles = makeStyles((theme) => ({
   root: {
     '& > *': {
       margin: theme.spacing(1),
     },
   },
   formControl: {
      margin: theme.spacing(1),
      minWidth: 300,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    error: {
       color: "crimson",
       fontSize: "bold"
    }
 }));

const AddMatieres = () => {
   //STATE Initial
  const initialMatiereState = {
    _id: null,
    nomMatiere: ""
  };

  //STATES
  const [matiere, setMatieres] = useState(initialMatiereState);
  const [submitted, setSubmitted] = useState(false);
  const [errorMessage, setErrorMessage] = useState("")
  const [open, setOpen] = React.useState(false);

  //Recupére les infos au changement
  const handleInputChange = event => {
    const { name, value } = event.target;
    setMatieres({ ...matiere, [name]: value });
  };

  // Fonction de save en BDD
  const saveMatiere = () => {
     /// VALIDATION
   // Si le champs est rempli
   if (matiere.nomMatiere) {
      const pattern = /^[a-zA-Z0-9_ éçè]*$/;
      const result = pattern.test(matiere.nomMatiere);
      //Si le test passe
      if (result === true) {
         //Initialisation des champs
         var data = {
            nomMatiere: matiere.nomMatiere
         };
      }
      // Sinon envoie du message d'erreur
      else {
         return setErrorMessage(`${matiere.nomMatiere} n'est pas un nom de matière valide`)
      }
   
    //Ajout de la donnée reçu dans les bon champs
    RoutesMatieres.create(data)
      .then(response => {
        setMatieres({
          id: response.data._id,
          nomMatiere: response.data.nomMatiere
        });
        setSubmitted(true);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
   }
   // Si le champs n'est pas rempli
   else {
      return setErrorMessage("Veuillez remplir le champs obligatoire")
  };
  }

  const newMatiere = () => {
    setMatieres(initialMatiereState);
    setSubmitted(false);
  };

 const handleClose = (event, reason) => {
  if (reason === 'clickaway') {
    return;
  }
  setOpen(false);
};

 
  const classesStyle = useStyles();

  return (
   <div className="submit-form">
   {submitted ? (
     <div>
          <div className={classesStyle.root}>
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success">
          Matière ajoutée avec succès!
        </Alert>
      </Snackbar>
      <Alert severity="success">Matière ajoutée avec succès!</Alert>
    
       <Button variant="contained" color="primary" onClick={newMatiere}>
        Ajouter une autre
      </Button>
      </div>
     </div>
   ) : (
     <div>
         <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="nomMatiere">Nom de la Matière</InputLabel>
            <Input id="nomMatiere" required={true} value={matiere.nomMatiere} onChange={handleInputChange} name="nomMatiere" placeholder="Nom de la matière" inputProps={{ 'aria-label': 'nom de la matière' }} />
            <FormHelperText className={classesStyle.error}>{errorMessage}</FormHelperText>
         </FormControl>

      <div className={classesStyle.root}>
      <Button variant="contained" color="primary" onClick={saveMatiere}>
        Ajouter
      </Button>
    </div>
     </div>
   )}

 </div>
);
};

export default AddMatieres;
