import React, { useState, useEffect } from "react";
import RoutesProfs from "../../../components/routes/RoutesProfs";
import RoutesClasses from "../../../components/routes/RoutesClasses";
import RoutesMatieres from "../../../components/routes/RoutesMatieres";
import {
   Input,
   FormControl,
   InputLabel,
   Button,
   Select,
   TextField,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';
import MuiAlert from '@material-ui/lab/Alert';

//Message sucess
function Alert(props) {
   return <MuiAlert elevation={6} variant="filled" {...props} />;
}

//Style
const useStyles = makeStyles((theme) => ({
   root: {
      '& > *': {
         margin: theme.spacing(1),
      },
   },
   formControl: {
      margin: theme.spacing(1),
      minWidth: 300,
   },
   selectEmpty: {
      marginTop: theme.spacing(2),
   },
   chips: {
      display: 'flex',
      flexWrap: 'wrap',
   },
   chip: {
      margin: 2,
   },
}));

 //Style Menu props multi select
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
   PaperProps: {
      style: {
         maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
         width: 250,
                        //Medias queries
                        "@media only screen and (min-device-width : 280px) and (max-device-width : 596px)": {
                           paddingTop: "10px",
                           width: "30em",
                           margin: "0",
                           overflow: "hidden"
                        },
                        "@media only screen and (min-device-width : 596px) and (max-device-width : 1322px)": {
                           maxWidth: "39em",
                           margin: "0"
                        },
      },
   },
};

const AddProf = () => {

   //STATE Initial
   const initialProfState = {
      _id: null,
      profNom: "",
      profPrenom: "",
      profTel: "",
      profMail: "",
      profMatiere: [],
      dateArrive: "",
      profClasses: []
   };

   //STATES
   const [prof, setProf] = useState(initialProfState);
   const [submitted, setSubmitted] = useState(false);
   const [classes, setClasse] = useState([]);
   const [matieres, setMatieres] = useState([]);
   const [selectedClasse, setSelectedClasse] = React.useState([]);
   const [selectedMatiere, setSelectedMatiere] = React.useState([]);
   const [errorMessage, setErrorMessage] = React.useState("");

useEffect(() => {
   retrieveClasses();
   retrieveMatieres();
}, []);

      //Récupération de toutes les classes
      const retrieveClasses = () => {
         RoutesClasses.getAll()
         .then(response => {
            setClasse(response.data.classes);
         })
         .catch(e => {
            console.log(e);
         });
      };

         //Récupération de toutes les matières
   const retrieveMatieres = () => {
      RoutesMatieres.getAll()
      .then(response => {
         setMatieres(response.data.matieres);
      })
      .catch(e => {
         console.log(e);
      });
   };

//Recupére les infos au changement
const handleInputChange = event => {
   const { name, value } = event.target;
   setProf({ ...prof, [name]: value });
};

  //Event du multi select
const handleChange = (event) => {
   setSelectedClasse(event.target.value);
};

   //Event du select matiere
   const handleChangeMatiere = (event) => {
      setSelectedMatiere(event.target.value);
   };

   // Fonction de save en BDD
   const saveProf = () => {
      //Initialisation des champs
      var data = {
         profNom: prof.profNom,
         profPrenom: prof.profPrenom,
         profTel: prof.profTel,
         profMail: prof.profMail,
         profMatiere: selectedMatiere,
         dateArrive: prof.dateArrive,
         profClasses: selectedClasse
      };

      //Tableau de message d'erreurs
      var messageErreur = "";
      const pattern = /^[a-zA-Z0-9ç_ ]*$/;
      const result = pattern.test(data.profNom);

      //Si le champs nom est propre -> validation prénom
      if (result === true) {
         const pattern = /^[a-zA-Z0-9_ ]*$/;
         const result = pattern.test(data.profPrenom);

         //Si le champs prénom est propre -> validation téléphone
         if (result === true) {
            const pattern = /^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/;
            const result = pattern.test(data.profTel);

            // Si le champs téléphone est propre -> validation email
            if (result === true) {
               const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$|^$/;
               const result = pattern.test(data.profMail);

               //Si le champs email est propre -> création du professeur
               if (result === true) {

                  //Ajout de la donnée reçu dans les bon champs
                  RoutesProfs.create(data)
                  .then(response => {
                     setSubmitted(true);
                  })
                  .catch(e => {
                     console.log(e);
                  });


               //Si le champ email est invalide
               } else {
                  messageErreur = "L'email du professeur n'est pas valide"
               }
            }

            //Si le champs téléphone est incorrect
            else {
               messageErreur = "Téléphone du professeur incorrect"
            }
         }

         //Si le champs prénom est incorrect
         else {
            messageErreur = "Prénom du professeur incorrect"
         }

       //Si le champs nom n'est pas propre
      } else {
         messageErreur = "Nom du professeur incorrect"
      }
      return setErrorMessage(messageErreur);
} //fin méthode save

const newProf = () => {
   setProf(initialProfState);
   setSubmitted(false);
};

const classesStyle = useStyles();

return (
   <div className="submit-form">
   {submitted ? (
      <div>
         <div className={classesStyle.root}>
            <Alert severity="success">Professeur ajouté avec succès!</Alert>
            <Button variant="contained" color="primary" onClick={newProf}>
            Ajouter un autre
            </Button>
         </div>
      </div>
   ) : (
      <div>
         {errorMessage ?
            (
            <Alert variant="filled" severity="error">
            {errorMessage}
            </Alert>
            )
         : false}
         <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="profNom">Nom</InputLabel>
            <Input id="profNom" required={true} value={prof.profNom} onChange={handleInputChange} name="profNom" placeholder="Nom" inputProps={{ 'aria-label': 'nom du professeur' }} />
         </FormControl>

         <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="profPrenom">Prénom</InputLabel>
            <Input id="profPrenom" required={true} value={prof.profPrenom} onChange={handleInputChange} name="profPrenom" placeholder="Prénom" inputProps={{ 'aria-label': 'prénom du professeur' }} />
         </FormControl>

         <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="profMail">Email</InputLabel>
            <Input id="profMail" type="email" required={true} value={prof.profMail} onChange={handleInputChange} name="profMail" placeholder="Email" inputProps={{ 'aria-label': 'email du professeur' }} />
         </FormControl>

         <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="profTel">Téléphone</InputLabel>
            <Input id="profTel" required={true} value={prof.profTel} onChange={handleInputChange} name="profTel" placeholder="Téléphone" inputProps={{ 'aria-label': 'téléphone du professeur' }} />
         </FormControl>

         <FormControl margin="normal" fullWidth>
         <TextField
         id="date"
         required={true}
         label="Date d'entrée"
         type="date"
         className={classesStyle.textField}
         InputLabelProps={{
            shrink: true,
         }}
         value={prof.dateArrive}
         onChange={handleInputChange}
         name="dateArrive"
         />
         </FormControl>

         <FormControl margin="normal" fullWidth>
         <InputLabel id="mutiple-chip-label">Matières</InputLabel>
         <Select
            labelId="mutiple-chip-label"
            id="demo-mutiple-chip"
            multiple={true}
            value={selectedMatiere}
            onChange={handleChangeMatiere}
            input={<Input id="select-multiple-chip" />}
            renderValue={(selected) => (
               <div className={classesStyle.chips}>
               {selected.map((value) => (
                  <Chip key={value} label={matieres.find(matiere => matiere._id === value).nomMatiere} className={classesStyle.chip} />
               ))}
               </div>
            )}
            MenuProps={MenuProps}
         >
            {matieres.map((matiere) => (
               <MenuItem key={matiere.nomMatiere} value={matiere._id} >
               {matiere.nomMatiere}
               </MenuItem>
            ))}
         </Select>
         </FormControl>

         <FormControl margin="normal" fullWidth>
         <InputLabel id="mutiple-chip-label">Classes</InputLabel>
         <Select
            labelId="mutiple-chip-label"
            id="demo-mutiple-chip"
            multiple={true}
            value={selectedClasse}
            onChange={handleChange}
            input={<Input id="select-multiple-chip" />}
            renderValue={(selected) => (
               <div className={classesStyle.chips}>
               {selected.map((value) => (
                  <Chip key={value} label={classes.find(classe => classe._id === value).classeEleve} className={classesStyle.chip} />
               ))}
               </div>
            )}
            MenuProps={MenuProps}
         >
            {classes.map((classe) => (
               <MenuItem key={classe.classeEleve} value={classe._id} >
               {classe.classeEleve}
               </MenuItem>
            ))}
         </Select>
         </FormControl>
         <div className={classesStyle.root}>
            <Button variant="contained" color="primary" onClick={saveProf}>
            Ajouter
            </Button>
         </div>
      </div>
   )}
   </div>
);
};

export default AddProf;
