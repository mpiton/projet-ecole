import React from 'react'
import RoutesProfs from "../../../components/routes/RoutesProfs";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import EditIcon from '@material-ui/icons/Edit';
import { LinearProgress, makeStyles, Modal } from '@material-ui/core';
import ProfDetail from "./ProfDetail"

function rand() {
   return Math.round(Math.random() * 20) - 10;
 }
 function getModalStyle() {
   const top = 50 + rand();
   const left = 50 + rand();
 
   return {
     top: `${top}%`,
     left: `${left}%`,
     transform: `translate(-${top}%, -${left}%)`,
   };
 }

 const useStyles = makeStyles((theme) => ({
   paper: {
     position: 'absolute',
     width: 400,
     backgroundColor: theme.palette.background.paper,
     border: '2px solid #000',
     boxShadow: theme.shadows[5],
     padding: theme.spacing(2, 4, 3),
   },
   tableau: {
      display: 'flex',
    flexWrap: "wrap",
    maxWidth: "100%",
            //smartphone
            "@media only screen and (min-device-width : 280px) and (max-device-width : 596px)": {
               maxWidth: "100%",
               margin: "0"
              },
        //smartphone
        "@media only screen and (min-device-width : 596px) and (max-device-width : 1322px)": {
         maxWidth: "100%",
         margin: "0"
        },
    //ipad
     "@media only screen and (min-device-width : 1322px) and (max-device-width : 1694px)": {
      maxWidth: "100%",
      marginLeft: "15%"
     },
     //Desktop & laptop
     "@media only screen and (min-width : 1694px)": {
      maxWidth: "100%",
     },
     //Large screen
     "@media only screen and (min-width : 1730px) ": {
      maxWidth: "100%",
     }
   }
 }));

export default function ProfTablo ({professeurs, retrieveProf, matieres}){

   const classes = useStyles();

   const [modalStyle] = React.useState(getModalStyle);
   const [open, setOpen] = React.useState(false);
   const [selectedProf, setSelectedProf] = React.useState();
   const [progress, setProgress] = React.useState(0);

   
   React.useEffect(() => {
      
      //Fake barre de progression
      const timer = setInterval(() => {
        setProgress((oldProgress) => {
          if (oldProgress === 100) {
            return 0;
          }
          const diff = Math.random() * 10;
          return Math.min(oldProgress + diff, 100);
        });
      }, 500);
      return () => {
         
        clearInterval(timer);
      };
    },[]);
 
   const handleOpen = () => {
     setOpen(true);
   };
 
   const handleClose = () => {
     setOpen(false);
   };

   const editProf = (prof) => {
      // console.log(prof);
      setSelectedProf(prof._id)
      handleOpen();
   }

   //Suppression d'un professeur
   const deleteProf = (prof) => {
      RoutesProfs.remove(prof._id)
      .then(response => {
         console.log(response.data);
         retrieveProf()
      })
      .catch(e => {
         console.log(e);
      });
   };



   const body = (
      <div style={modalStyle} className={classes.paper}>
        <ProfDetail props={selectedProf} />
      </div>
    );

   //Quand ça charge...
   while (professeurs.length <= 0) {
      return <LinearProgress variant="determinate" value={progress} />
   }
   //Chargement fini, affichage:
   return (
      <div className={classes.tableau}>
         <div className="col-md-12">
            <div className="card">
               <div className="card-body">
                  <h5 className="card-title text-uppercase mb-0">Professeurs</h5>
               </div>
            <div className="table-responsive">
            <table className="table no-wrap user-table mb-0">
               <thead>
                  <tr>
                     <th scope="col" className="border-0 text-uppercase font-medium">#</th>
                     <th scope="col" className="border-0 text-uppercase font-medium">Nom</th>
                     <th scope="col" className="border-0 text-uppercase font-medium">Matière</th>
                     <th scope="col" className="border-0 text-uppercase font-medium">Contact</th>
                     <th scope="col" className="border-0 text-uppercase font-medium">Ajouté</th>
                     <th scope="col" className="border-0 text-uppercase font-medium">Manage</th>
                  </tr>
               </thead>
               <tbody>
                  {professeurs.map((prof, index) => (
                     <tr key={prof._id}>
                        <td className="pl-4">{index + 1}</td>
                        <td>
                           <h5 className="font-medium mb-0"> {prof.profNom}</h5>
                           <span className="text-muted">{prof.profPrenom}</span>
                        </td>
                        <td>
                           {prof.profMatiere.map((profMatiere, index) => (
                              <React.Fragment key={index}>
                              <span className="text-muted">{matieres.find(matiere => matiere._id === profMatiere).nomMatiere}</span><br />
                              </React.Fragment>
                           ))}
                        </td>
                        <td>
                           <span className="text-muted">{prof.profMail}</span><br />
                           <span className="text-muted">{prof.profTel}</span>
                        </td>
                        <td>
                           <span className="text-muted">{new Date(prof.dateArrive).toLocaleDateString("fr-FR")}</span><br />
                  <span className="text-muted">{new Date(prof.dateArrive).toJSON().substring(10,16).replace('T',' ')}</span>
                        </td>
                        <td>
                           <button onClick={() => deleteProf(prof)} type="button" className="btn btn-outline-info ml-2"><DeleteForeverIcon/> </button>
                           <button onClick={() => editProf(prof)} type="button" className="btn btn-outline-info ml-2"><EditIcon /> </button>
                           <Modal
                           open={open}
                           onClose={handleClose}
                           aria-labelledby="simple-modal-title"
                           aria-describedby="simple-modal-description"
                           >
                           {body}
                           </Modal>
                        </td>
                     </tr>
                  ))}
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>
)
}
