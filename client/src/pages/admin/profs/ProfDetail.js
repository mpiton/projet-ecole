import React, { useState, useEffect } from "react";
import RoutesProfs from "../../../components/routes/RoutesProfs";
import { Avatar, Button, Chip, FormControl, Input, InputLabel, makeStyles, MenuItem, Select } from "@material-ui/core";
import RoutesClasses from "../../../components/routes/RoutesClasses";
import RoutesMatieres from "../../../components/routes/RoutesMatieres";
import Alert from "@material-ui/lab/Alert";

//Style
const useStyles = makeStyles((theme) => ({
   root: {
      '& .MuiTextField-root': {
         margin: theme.spacing(1),
         width: '25ch',
      },
   },
   formControl: {
      margin: theme.spacing(1),
      minWidth: 300,
   },
   selectEmpty: {
      marginTop: theme.spacing(2),
   },
   chips: {
      display: 'flex',
      flexWrap: 'wrap',
   },
   chip: {
      margin: 2,
   },
   delete: {
      backgroundColor: "crimson",
      marginRight: "20px",
   },
   bouton: {
      marginTop: theme.spacing(1),
      marginBottom: "30px",
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'center'
   }
}));
//Style Menu props multi select
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
   PaperProps: {
      style: {
         maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
         width: 250,
      },
   },
};

const Prof = props => {

   //déclaration du style
   const classesStyle = useStyles();

   //Récupération de l'id en string
   var maProps = props.props

   //Initialisation de mon state
   const initialProfState = {
      _id: null,
      profNom: "",
      profPrenom: "",
      profTel: "",
      dateArrive: "",
      /*adresse: {
         codePostal: "", //TODO: à ajouter dans le futur
         rue: "",
         ville: ""
      },*/
      profMail: "",
      profMatiere: [],
      profClasses: []
   };

   //STATES
   const [currentProf, setCurrentProf] = useState(initialProfState);
   const [message, setMessage] = useState("");
   const [selectedClasse, setSelectedClasse] = React.useState([]);
   const [classes, setClasse] = useState([]);
   const [matieres, setMatieres] = useState([]);
   const [selectedMatiere, setSelectedMatiere] = React.useState([]);
   const [error, setError] = useState("");

   //Méthode de récupération du professeur
   const getProf = id => {
      RoutesProfs.get(id)
      .then(response => {
         setCurrentProf(response.data.prof);
         // console.log(response.data.prof);
      })
      .catch(e => {
         console.log(e);
      });
   };

   useEffect(() => {
      getProf(maProps);
      retrieveClasses();
      retrieveMatieres();
   // eslint-disable-next-line react-hooks/exhaustive-deps
   }, []);

   //Récupération de toutes les classes
   const retrieveClasses = () => {
      RoutesClasses.getAll()
      .then(response => {
         setClasse(response.data.classes);
      })
      .catch(e => {
         console.log(e);
      });
   };

   //Récupération de toutes les matières
   const retrieveMatieres = () => {
      RoutesMatieres.getAll()
      .then(response => {
         setMatieres(response.data.matieres);
         console.log(matieres);
      })
      .catch(e => {
         console.log(e);
      });
   };

   //écouteur sur les inputs
   const handleInputChange = event => {
      const { name, value } = event.target;
      setCurrentProf({ ...currentProf, [name]: value });
   };
   //ecouteur matieres
   const handleChangeMatiere = (event) => {
      setSelectedMatiere(event.target.value);
   };
   //ecouteur classe
   const handleChange = (event) => {
   setSelectedClasse(event.target.value);
   };


   //méthode de mise à jour du professeur
   const updateProf = () => {
      //Validation nom
      const nom = /^[a-zA-Z_ ]*$/;
      const resultNom = nom.test(currentProf.profNom)
      if (resultNom === false) {
         return setError("Le nom du professeur est invalide")
      }
      //Validation prénom
      const prenom = /^[a-zA-Z_ ]*$/;
      const resultPrenom = prenom.test(currentProf.profPrenom)
      if (resultPrenom === false) {
         return setError("Le prénom du professeur est invalide")
      }
      //Validation Email
      const email = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      const resultEmail = email.test(currentProf.profMail)
      if (resultEmail === false) {
         return setError("L'email du professeur est invalide")
      }
      //Validation Téléphone
      const tel = /^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/;
      const resultTel = tel.test(currentProf.profTel)
      if (resultTel === false) {
         return setError("Le téléphone du professeur est invalide")
      }
      //reinitialisation du state erreur
      setError("")
      //Initialisation des champs
      var data = {
         profNom: currentProf.profNom,
         profPrenom: currentProf.profPrenom,
         profTel: currentProf.profTel,
         profMail: currentProf.profMail,
         profMatiere: selectedMatiere,
         profClasses: selectedClasse
      };
      RoutesProfs.update(currentProf._id, data)
      .then(response => {
         console.log(response.data);
         setMessage("Les informations ont été mise à jour");
      })
      .catch(e => {
         console.log(e);
      });
   };

   //méthode de suppression du professeur
   const deleteProf = () => {
      RoutesProfs.remove(currentProf._id)
      .then(response => {
         console.log(response.data);
         props.history.push("/liste-professeurs");
      })
      .catch(e => {
        console.log(e);
      });
  };

  //Affichage
  return (
   <div>
     <div className="edit-form">
       <form className={classesStyle.root} noValidate autoComplete="off">
      <div>
<Avatar alt={currentProf.profNom + currentProf.profPrenom} src={currentProf.profPhoto} />
{error ? <Alert severity="error">{error}</Alert> : null}
        <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="profNom">Nom</InputLabel>
            <Input id="profNom" required={true} value={currentProf.profNom} onChange={handleInputChange} name="profNom" placeholder="Nom" inputProps={{ 'aria-label': 'nom du professeur' }} />
         </FormControl>

         <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="profPrenom">Prénom</InputLabel>
            <Input id="profPrenom" required={true} value={currentProf.profPrenom} onChange={handleInputChange} name="profPrenom" placeholder="Prénom" inputProps={{ 'aria-label': 'prénom du professeur' }} />
         </FormControl>

         <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="profMail">Email</InputLabel>
            <Input id="profMail" type="email" required={true} value={currentProf.profMail} onChange={handleInputChange} name="profMail" placeholder="Email" inputProps={{ 'aria-label': 'email du professeur' }} />
         </FormControl>


        <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="profTel">Téléphone</InputLabel>
            <Input id="profTel" required={true} value={currentProf.profTel} onChange={handleInputChange} name="profTel" placeholder="Téléphone" inputProps={{ 'aria-label': 'téléphone du professeur' }} />
         </FormControl>

         <FormControl className={classesStyle.formControl}>
        <InputLabel id="mutiple-chip-label">Matières</InputLabel>
        <Select
          labelId="mutiple-chip-label"
          id="demo-mutiple-chip"
        multiple={true}
          value={selectedMatiere}
          onChange={handleChangeMatiere}
          input={<Input id="select-multiple-chip" />}
          renderValue={(selected) => (
            <div className={classesStyle.chips}>
               
              {selected.map((value) => (
                <Chip key={value} label={matieres.find(matiere => matiere._id === value).nomMatiere} className={classesStyle.chip} />
              ))}
            </div>
          )}
          MenuProps={MenuProps}
        >
          {matieres.map((matiere) => (
             
            <MenuItem key={matiere.nomMatiere} value={matiere._id} >
              {matiere.nomMatiere}
            </MenuItem>
          ))}
        </Select>
      </FormControl>

      <FormControl className={classesStyle.formControl}>
        <InputLabel id="mutiple-chip-label">Classes</InputLabel>
        <Select
          labelId="mutiple-chip-label"
          id="demo-mutiple-chip"
         multiple={true}
          value={selectedClasse}
          onChange={handleChange}
          input={<Input id="select-multiple-chip" />}
          renderValue={(selected) => (
            <div className={classesStyle.chips}>
              {selected.map((value) => (
                <Chip key={value} label={classes.find(classe => classe._id === value).classeEleve} className={classesStyle.chip} />
              ))}
            </div>
          )}
          MenuProps={MenuProps}
        >
          {classes.map((classe) => (
            <MenuItem key={classe.classeEleve} value={classe._id} >
              {classe.classeEleve}
            </MenuItem>
          ))}
        </Select>
      </FormControl>


         </div>
       </form>
   
         <div className={classesStyle.bouton}>
          <Button variant="contained" onClick={deleteProf} className={classesStyle.delete}>
  Supprimer
</Button>
<Button type="submit" variant="contained" color="primary" onClick={updateProf}>
  Mettre à jour
</Button>
</div>
{message ? <Alert severity="success">{message}</Alert> : null}
        </div>
    </div>
  );
};

export default Prof;
