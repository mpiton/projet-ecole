import React from 'react'

import { makeStyles } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';


const useStyles = makeStyles((theme) => ({
   root: {
     '& > *': {
         marginTop: theme.spacing(2),
      },
   },
}));

export default function Paginator({change, profParPage, totalProfs}) {
   const nbPages = [];

   for (let index = 1; index <= Math.ceil(totalProfs / profParPage); index++) {
      nbPages.push(index);
   }

   const classes = useStyles();
   return (
      <div className={classes.root}>
         <Pagination onChange={change} count={nbPages.length} color="primary" />
      </div>
   );
}
