import React, { useState, useEffect } from "react";
import RoutesMatieres from "../../../components/routes/RoutesMatieres";
import '../../../css/ProfListe.css'
import { Link } from "react-router-dom";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import EditIcon from '@material-ui/icons/Edit';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import AddMatiere from "../profs/AddMatiere"
import { Button } from "@material-ui/core";


const GestionMatieres = () => {
   //STATES
  const [matieres, setMatieres] = useState([]);
  const [loading, setLoading] = useState(false);
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);

  
//Style Modal
  function rand() {
   return Math.round(Math.random() * 20) - 10;
 }
 
 //Position de la modal
 function getModalStyle() {
   const top = 50 + rand();
   const left = 50 + rand();
 
   return {
     top: `${top}%`,
     left: `${left}%`,
     transform: `translate(-${top}%, -${left}%)`,
   };
 }

  const useStyles = makeStyles((theme) => ({
   paper: {
     position: 'absolute',
     width: 400,
     backgroundColor: theme.palette.background.paper,
     border: '2px solid #000',
     boxShadow: theme.shadows[5],
     padding: theme.spacing(2, 4, 3),
   },
 }));

const classesStyle = useStyles();

 //Ouverture de la modal
  const handleOpen = () => {
    setOpen(true);
  };

  //Fermeture de la modal
  const handleClose = () => {
    setOpen(false);
    retrieveMatieres();
  };

  //Contenu de la modal
  const body = (
    <div style={modalStyle} className={classesStyle.paper}>
      <AddMatiere />
    </div>
  );


  useEffect(() => {
   retrieveMatieres();
 }, []);

   //Récupération de toutes les matières
   const retrieveMatieres = () => {
   setLoading(true)
   RoutesMatieres.getAll()
   .then(response => {
      setMatieres(response.data.matieres);
      setLoading(false)
   })
   .catch(e => {
      console.log(e);
   });
};

      //Suppression d'un élève
      const deleteMatiere = (matiere) => {
         RoutesMatieres.remove(matiere._id)
         .then(response => {
            console.log(response.data);
            retrieveMatieres()
         })
         .catch(e => {
            console.log(e);
         });
      };

      
   
      //Quand ça charge...
      if (loading) {
         return <h2>Chargement...</h2>
      }
      if (matieres.length <= 0) {
         return (
            <div>
               <h3>Pas de matières trouvée</h3>
            <Button variant="contained" color="primary" onClick={handleOpen}>Ajouter une Matière</Button>
            <Modal
              open={open}
              onClose={handleClose}
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
            >
              {body}
            </Modal>
          </div>
            )
      }
      //Chargement fini, affichage:
      return (
         
         <div className="row">
            <div className="col-md-12">
               <div className="card">
                  <div className="card-body">
                     <h5 className="card-title text-uppercase mb-0">Matières</h5>
                  </div>
               <div className="table-responsive">
               <div>
      <Button variant="contained" color="primary" onClick={handleOpen}>Ajouter une Matière</Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
               <table className="table no-wrap user-table mb-0">
                  <thead>
                     <tr>
                        <th scope="col" className="border-0 text-uppercase font-medium">#</th>
                        <th scope="col" className="border-0 text-uppercase font-medium">Nom de la matière</th>
                        <th scope="col" className="border-0 text-uppercase font-medium">Manage</th>
                     </tr>
                  </thead>
                  <tbody>
                     {matieres.map((matiere, index) => (
                        <tr key={matiere._id}>
                           <td className="pl-4">{index + 1}</td>
                           <td>
                              <h5 className="font-medium mb-0"> {matiere.nomMatiere}</h5>
                           </td>
                           <td>
                              <button onClick={() => deleteMatiere(matiere)} type="button" className="btn btn-outline-info ml-2"><DeleteForeverIcon/></button>
                              <Link to={"/matiere/" + matiere._id} >
                                 <button type="button" className="btn btn-outline-info ml-2"><EditIcon /></button>
                              </Link>
                              </td>
                        </tr>
                     ))}
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   )
};


export default GestionMatieres;
