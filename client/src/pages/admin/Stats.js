import React from 'react'
import Presence from "../graphiques/Presence"
import NbEleveParClasse from "../graphiques/NbEleveParClasse"
import LastEvents from "../graphiques/LastEvents"
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
root: {
   display: 'flex',
 flexWrap: "wrap",
 maxWidth: "100%",
 overflow: "hidden",
 justifyContent: "space-between",
         //smartphone
         "@media only screen and (min-device-width : 280px) and (max-device-width : 596px)": {
            maxWidth: "100%",
            margin: "0"
           },
     //smartphone
     "@media only screen and (min-device-width : 596px) and (max-device-width : 1322px)": {
      maxWidth: "100%",
      margin: "0",
      textAlign: "center"
     },
 //ipad
  "@media only screen and (min-device-width : 1322px) and (max-device-width : 1694px)": {
   maxWidth: "100%",
   marginLeft: "15%",
   justifyContent: "center"
  },
  //Desktop & laptop
  "@media only screen and (min-width : 1694px)": {
   maxWidth: "100%",
  },
}
}));

export default function Stats() {
   const classes = useStyles();
   return (
      <div className={classes.root}>
         <Presence/>
         <LastEvents/>
         <NbEleveParClasse />
      </div>
   )
}
