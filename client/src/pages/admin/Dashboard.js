import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {Dashboard, School, AssignmentInd, PersonAdd, Assessment, Assignment, Settings, ExitToApp} from '@material-ui/icons';
import ForumIcon from '@material-ui/icons/Forum';
import logo from "../../images/logo.png";
import Breadcrumbs from "./Breadcrumbs"
import { Modal } from '@material-ui/core';
import AddProf from "./profs/AddProf";

const drawerWidth = 250;

function rand() {
   return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
   const top = 50 + rand();
   const left = 50 + rand();
   return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
   };
}

const useStyles = makeStyles((theme) => ({
   root: {
      display: 'flex',
   },
   appBar: {
      zIndex: theme.zIndex.drawer + 1,
   },
   drawer: {
      width: drawerWidth,
      flexShrink: 0,
         //smartphone
      "@media only screen and (min-device-width : 480px) and (max-device-width : 1612px)": {
         width:"0%",
         marginTop: "-2500px"
      },
   },
   drawerOpen: {
      width: drawerWidth,
      flexShrink: 0,
         //smartphone
      "@media only screen and (min-device-width : 480px) and (max-device-width : 1612px)": {
         width:"100%",
      },
   },
   drawerPaper: {
      width: drawerWidth,
      //smartphone
      "@media only screen and (min-device-width : 280px) and (max-device-width : 1612px)": {
      // display: 'none', //none by  default
      width:"0%",
      },
   },
   drawerPaperOpen: {
   width: drawerWidth,
      //smartphone
      "@media only screen and (min-device-width : 280px) and (max-device-width : 1612px)": {
      // display: 'none', //none by  default
      width:"100vw",
   },
   },
   drawerContainer: {
      overflow: 'auto',
   },
   content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      "@media only screen and (min-device-width : 280px) and (max-device-width : 320px)": {
         paddingTop: theme.spacing(3),
         paddingBottom: theme.spacing(3),
         paddingRight: "0",
         paddingLeft: "0"
      },
   },
   paper: {
      position: 'absolute',
      width: 400,
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
   },
   hamburger: {
      height: "30px",
      width: "30px",
      marginTop: "5px",
      cursor: "pointer",
      position: "absolute",
      right: "20%",
      display: "none",
      "@media only screen and (min-device-width : 279px) and (max-device-width : 320px)": {
         display: 'inline-block',
         position: "absolute",
         right: "5%"
      },
      //smartphone
      "@media only screen and (min-device-width : 320px) and (max-device-width : 1612px)": {
            display: 'inline-block',
            position: "absolute",
            right: "20%"
      },
   },
   hamburSpan: {
      height: "3px",
      backgroundColor: theme.palette.background.paper,
      width: "100%",
      position: "absolute",
      top: "20px",
      left: "0",
      "&::before": {
         content: '""',
         height: "3px",
         backgroundColor: theme.palette.background.paper,
         width: "100%",
         position: "absolute",
         top: "-10px",
         left: "0",
      },
      "&::after": {
         content: '""',
         height: "3px",
         backgroundColor: theme.palette.background.paper,
         width: "100%",
         position: "absolute",
         top: "10px",
         left: "0",
      }
   }
}));

function ListItemLink(props) {
   return <ListItem button component="a" {...props} />;
}

export default function ClippedDrawer() {
   const classes = useStyles();

   const [modalStyle] = React.useState(getModalStyle);
   const [open, setOpen] = React.useState(false);
   const [openMenu, setOpenMenu] = React.useState(false)

   const handleOpen = () => {
      setOpen(true);
   };

   const toggleMenu = () => {
      if (openMenu === true) {
         setOpenMenu(false);
      }
      else {
         setOpenMenu(true);
      }
   //console.log(openMenu);
};

   const handleClose = () => {
      setOpen(false);
   };

   const body = (
      <div style={modalStyle} className={classes.paper}>
         <AddProf/>
      </div>
   );

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <Typography variant="h6" noWrap>
          <img src={logo} style={{marginRight:"16px"}} width="40px" height="40px" alt="logo de l'école"/>
          <Typography variant="h4" display="inline">&#201;cole 404</Typography>
          <div onClick={toggleMenu} className={classes.hamburger}>
            <span className={classes.hamburSpan}></span>
        </div>
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        className={openMenu ? classes.drawerOpen : classes.drawer}
        variant="permanent"
        classes={
         openMenu ? {paper: classes.drawerPaperOpen} : {paper: classes.drawerPaper}
       }
      >
        <Toolbar />
        {/* MENU DE GAUCHE */}
        <div className={classes.drawerContainer}>
          <List>
              <ListItem button>
              <ListItemLink style={{backgroundColor: 'transparent', padding: 0}} href="/">
                <ListItemIcon>
                     <Dashboard/>
                </ListItemIcon>
                <ListItemText primary="Tableau de bord" />
                </ListItemLink>
              </ListItem>
          </List>
          <Divider />
          {/* GESTION PROFESSEURS */}
          <List>
              <ListItem button>
                 <ListItemLink style={{backgroundColor: 'transparent', padding: 0}} href="/liste-professeurs">
                <ListItemIcon>
                     <AssignmentInd/>
                </ListItemIcon>
                <ListItemText primary="Liste de professeurs" />
                </ListItemLink>
              </ListItem>
              <ListItem button>
              <ListItemLink style={{backgroundColor: 'transparent', padding: 0}} href="/reunions">
                <ListItemIcon>
                     <ForumIcon/>
                </ListItemIcon>
                <ListItemText primary="Réunions" />
                </ListItemLink>
              </ListItem>
              <ListItem button>
              <ListItemLink style={{backgroundColor: 'transparent', padding: 0}} href="/emploidutemps">
                <ListItemIcon>
                     <Assignment/>
                </ListItemIcon>
                <ListItemText primary="Emploi du temps" />
                </ListItemLink>
              </ListItem>
              <ListItem button>
              <ListItemLink onClick={handleOpen} style={{backgroundColor: 'transparent', padding: 0}} href="#">
                <ListItemIcon>
                     <PersonAdd/>
                </ListItemIcon>
                <ListItemText primary="Ajouter un professeur" />
                </ListItemLink>
              </ListItem>
          </List>
          <Divider />
          {/* ADMINISTRATION */}
          <List>
          <ListItem button>
               <ListItemLink style={{backgroundColor: 'transparent', padding: 0}} href="/add-matiere">
                <ListItemIcon>
                     <PersonAdd/>
                </ListItemIcon>
                <ListItemText primary="Gestion Matières" />
                </ListItemLink>
                </ListItem>
                <ListItem button>
              <ListItemLink style={{backgroundColor: 'transparent', padding: 0}} href="/add-classe">
                <ListItemIcon>
                     <PersonAdd/>
                </ListItemIcon>
                <ListItemText primary="Gestion Classes" />
                </ListItemLink>
              </ListItem>
          </List>
          <Divider />
          {/* GESTION ELEVES */}
          <List>
              <ListItem button>
              <ListItemLink style={{backgroundColor: 'transparent', padding: 0}} href="/liste-eleves">
                <ListItemIcon>
                     <School/>
                </ListItemIcon>
                <ListItemText primary="Liste des élèves" />
                </ListItemLink>
              </ListItem>
              <ListItem button>
              <ListItemLink style={{backgroundColor: 'transparent', padding: 0}} href="/notes">
                <ListItemIcon>
                     <Assessment/>
                </ListItemIcon>
                <ListItemText primary="Notes" />
                </ListItemLink>
              </ListItem>
              <ListItem button>
              <ListItemLink style={{backgroundColor: 'transparent', padding: 0}} href="/add-eleve">
                <ListItemIcon>
                     <PersonAdd/>
                </ListItemIcon>
                <ListItemText primary="Ajouter" />
                </ListItemLink>
              </ListItem>
          </List>
          <Divider/>
          <List>
              <ListItem button>
              <ListItemLink style={{backgroundColor: 'transparent', padding: 0}} href="/settings">
                <ListItemIcon>
                     <Settings/>
                </ListItemIcon>
                <ListItemText primary="Paramètres" />
                </ListItemLink>
              </ListItem>
          </List>
          <Divider />
          <List>
              <ListItem button>
              <ListItemLink style={{backgroundColor: 'transparent', padding: 0}} href="/logout">
                <ListItemIcon>
                     <ExitToApp/>
                </ListItemIcon>
                <ListItemText primary="Déconnexion" />
                </ListItemLink>
              </ListItem>
          </List>
        </div>
      </Drawer>
      <main className={classes.content}>
        <Toolbar />
        <Breadcrumbs />
        <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
      </main>
    </div>
  );
}
