import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import HomeIcon from '@material-ui/icons/Home';

const useStyles = makeStyles((theme) => ({
  link: {
    display: 'flex',
                //smartphone
                "@media only screen and (min-device-width : 280px) and (max-device-width : 480px)": {
                  display: 'none',
                 },
            //smartphone
        "@media only screen and (min-device-width : 480px) and (max-device-width : 1322px)": {
         display: 'none',
        },
    //ipad
     "@media only screen and (min-device-width : 1322px) and (max-device-width : 1694px)": {
      maxWidth: "100%",
     },
     //Desktop & laptop
     "@media only screen and (min-width : 1694px)": {
      maxWidth: "100%",
     },
     //Large screen
     "@media only screen and (min-width : 1730px) ": {
      maxWidth: "100%",
     }
  },
  icon: {
    marginRight: theme.spacing(0.5),
    width: 20,
    height: 20,
          //smartphone
          "@media only screen and (min-device-width : 280px) and (max-device-width : 480px)": {
            display: "none"
           },
      //smartphone
                "@media only screen and (min-device-width : 480px) and (max-device-width : 1322px)": {
                  margin: "0"
                 },
             //ipad
              "@media only screen and (min-device-width : 1322px) and (max-device-width : 1694px)": {
               margin: "0"
              },
  },
}));

function handleClick(event) {
  event.preventDefault();
}

export default function IconBreadcrumbs() {
  const classes = useStyles();

  return (
    <Breadcrumbs aria-label="breadcrumb" className={classes.link}>
      <Link color="inherit" href="/" onClick={handleClick} className={classes.link}>
        <HomeIcon className={classes.icon} />
        Tableau de bord
      </Link>
      <Link
        color="inherit"
        href="#"
        onClick={handleClick}
        className={classes.link}
      >
        Liste des professeurs
      </Link>
    </Breadcrumbs>
  );
}
