import React, { useState, useEffect } from "react";
import RoutesEleves from "../../../components/routes/RoutesEleves";
import {
   Input, 
   FormControl,
   InputLabel,
   Button,
   Select,
   TextField
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import RoutesClasses from "../../../components/routes/RoutesClasses";

const useStyles = makeStyles((theme) => ({
   root: {
     '& > *': {
       margin: theme.spacing(1),
     },
   },
   formControl: {
      margin: theme.spacing(1),
      minWidth: 300,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
 }));



const AddEleve = () => {
   //STATE Initial
  const initialEleveState = {
    _id: null,
    eleNom: "",
    elePrenom: "",
    eleTel: "",
    eleTelSec: "",
    eleGrade: "",
    eleBirthday: "",
    eleAdresse: "",
    eleComment: "",
    eleClasse: null
  };

  //STATES
  const [eleve, setEleve] = useState(initialEleveState);
  const [submitted, setSubmitted] = useState(false);
  const [classes, setClasse] = useState([]);

  //Recupére les infos au changement
  const handleInputChange = event => {
    const { name, value } = event.target;
    setEleve({ ...eleve, [name]: value });
  };


  useEffect(() => {
   retrieveClasses();
 }, []);

      //Récupération de toutes les classes
      const retrieveClasses = () => {
         RoutesClasses.getAll()
         .then(response => {
            setClasse(response.data.classes);
         })
         .catch(e => {
            console.log(e);
         });
      };


  // Fonction de save en BDD
  const saveEleve = () => {
     //Initialisation des champs
    var data = {
      eleNom: eleve.eleNom,
      elePrenom: eleve.elePrenom,
      eleTel: eleve.eleTel,
      eleTelSec: eleve.eleTelSec,
      eleGrade: eleve.eleGrade,
      eleBirthday: eleve.eleBirthday,
      eleAdresse: eleve.eleAdresse,
      eleComment: eleve.eleComment,
      eleClasse: classes
    };
    //Ajout de la donnée reçu dans les bon champs
    RoutesEleves.create(data)
      .then(response => {
        setEleve({
          id: response.data._id,
          eleNom: response.data.eleNom,
          elePrenom: response.data.elePrenom,
          eleTel: response.data.eleTel,
          eleTelSec: response.data.eleTelSec,
          eleGrade: response.data.eleGrade,
          eleBirthday: response.data.eleBirthday,
          eleAdresse: response.data.eleAdresse,
          eleComment: response.data.eleComment,
          eleClasse: response.data.eleClasse

        });
        setSubmitted(true);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  };


  const newEleve = () => {
    setEleve(initialEleveState);
    setSubmitted(false);
  };

  const classesStyle = useStyles();

  return (
   <div className="submit-form">
   {submitted ? (
     <div>
       <h4>L'élève a été ajouté!</h4>
       <Button variant="contained" color="primary" onClick={newEleve}>
        Ajouter un autre
      </Button>
     </div>
   ) : (
     <div>
         <FormControl className={classesStyle.formControl}>
            <InputLabel htmlFor="eleNom">Nom</InputLabel>
            <Input id="eleNom" required={true} value={eleve.eleNom} onChange={handleInputChange} name="eleNom" placeholder="Nom" inputProps={{ 'aria-label': 'nom de l\'élève' }} />
            </FormControl>

            <FormControl className={classesStyle.formControl}>
            <InputLabel htmlFor="elePrenom">Prénom</InputLabel>
            <Input id="elePrenom" required={true} value={eleve.elePrenom} onChange={handleInputChange} name="elePrenom" placeholder="Prénom" inputProps={{ 'aria-label': 'prénom de l\'élève' }} />
            </FormControl>

            <FormControl className={classesStyle.formControl}>
            <InputLabel htmlFor="eleAdresse">Adresse</InputLabel>
            <Input id="eleAdresse" required={true} value={eleve.eleAdresse} onChange={handleInputChange} name="eleAdresse" placeholder="Adresse" inputProps={{ 'aria-label': 'Adresse de l\'élève' }} />
            </FormControl>

            <FormControl className={classesStyle.formControl}>
            <InputLabel htmlFor="eleTel">Téléphone 1</InputLabel>
            <Input id="eleTel" type="tel" required={true} value={eleve.eleTel} onChange={handleInputChange} name="eleTel" placeholder="Téléphone 1" inputProps={{ 'aria-label': 'téléphone principal de l\'élève' }} />
            </FormControl>

            <FormControl className={classesStyle.formControl}>
            <InputLabel htmlFor="eleTelSec">Téléphone 2</InputLabel>
            <Input type="tel" id="eleTelSec" required={true} value={eleve.eleTelSec} onChange={handleInputChange} name="eleTelSec" placeholder="Téléphone 2" inputProps={{ 'aria-label': 'téléphone secondaire de l\'élève' }} />
            </FormControl>

      <FormControl className={classesStyle.formControl}>
      <TextField
        id="date"
        required={true}
        label="Date de naissance"
        type="date"
        className={classesStyle.textField}
        InputLabelProps={{
          shrink: true,
        }}
        value={eleve.eleBirthday}
        onChange={handleInputChange}
        name="eleBirthday"
      />
      </FormControl>

<FormControl className={classesStyle.formControl}>
        <InputLabel htmlFor="eleClasse">Classe</InputLabel>
        <Select
          required={true}
          native
          value={classes.classeEleve}
          onChange={handleInputChange}
        >
               <option aria-label="Choisissez" value="" />
               {classes.map((classe, index) => (
               <option key={index} value={classe.classeEleve} name={classe.classeEleve}>{classe.classeEleve}</option>
               ))}
        </Select>
      </FormControl>

      <div className={classesStyle.root}>
      <Button variant="contained" color="primary" onClick={saveEleve}>
        Ajouter
      </Button>
    </div>
     </div>
   )}

 </div>
);
};

export default AddEleve;
