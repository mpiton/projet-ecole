import React, { useState, useEffect } from "react";
import RoutesClasses from "../../../components/routes/RoutesClasses";
import '../../../css/ProfListe.css'
import { Link } from "react-router-dom";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import EditIcon from '@material-ui/icons/Edit';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import AddClasses from "../eleves/AddClasses"

const GestionClasses = () => {
  const [classes, setClasses] = useState([]);
  const [loading, setLoading] = useState(false);


  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);

  

  function rand() {
   return Math.round(Math.random() * 20) - 10;
 }
 
 //Position de la modal
 function getModalStyle() {
   const top = 50 + rand();
   const left = 50 + rand();
 
   return {
     top: `${top}%`,
     left: `${left}%`,
     transform: `translate(-${top}%, -${left}%)`,
   };
 }

 
  const useStyles = makeStyles((theme) => ({
   paper: {
     position: 'absolute',
     width: 400,
     backgroundColor: theme.palette.background.paper,
     border: '2px solid #000',
     boxShadow: theme.shadows[5],
     padding: theme.spacing(2, 4, 3),
   },
 }));

const classesStyle = useStyles();

 //Ouverture de la modal
  const handleOpen = () => {
    setOpen(true);
  };

  //Fermeture de la modal
  const handleClose = () => {
    setOpen(false);
    retrieveClasses();
  };

  //Contenu de la modal
  const body = (
    <div style={modalStyle} className={classesStyle.paper}>
      <h2 id="simple-modal-title">Ajoutez une classe</h2>
      <AddClasses />
    </div>
  );


  useEffect(() => {
   retrieveClasses();
 }, []);

   //Récupération de toutes les classes
   const retrieveClasses = () => {
   setLoading(true)
   RoutesClasses.getAll()
   .then(response => {
      setClasses(response.data.classes);
      setLoading(false)
   })
   .catch(e => {
      console.log(e);
   });
};

      //Suppression d'un élève
      const deleteClasse = (classe) => {
         RoutesClasses.remove(classe._id)
         .then(response => {
            console.log(response.data);
            retrieveClasses()
         })
         .catch(e => {
            console.log(e);
         });
      };

      
   
      //Quand ça charge...
      if (loading) {
         return <h2>Chargement...</h2>
      }
      if (classes.length <= 0) {
         return (
            <div>
               <h3>Pas de classe trouvée</h3>
            <button type="button" onClick={handleOpen}>
              Ajouter une classe
            </button>
            <Modal
              open={open}
              onClose={handleClose}
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
            >
              {body}
            </Modal>
          </div>
            )
      }
      //Chargement fini, affichage:
      return (
         
         <div className="row">
            <div className="col-md-12">
               <div className="card">
                  <div className="card-body">
                     <h5 className="card-title text-uppercase mb-0">Classes</h5>
                  </div>
               <div className="table-responsive">
               <div>
      <button type="button" onClick={handleOpen}>
        Ajouter une classe
      </button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
               <table className="table no-wrap user-table mb-0">
                  <thead>
                     <tr>
                        <th scope="col" className="border-0 text-uppercase font-medium">#</th>
                        <th scope="col" className="border-0 text-uppercase font-medium">Nom de la classe</th>
                        <th scope="col" className="border-0 text-uppercase font-medium">Manage</th>
                     </tr>
                  </thead>
                  <tbody>
                     {classes.map((classe, index) => (
                        <tr key={classe._id}>
                           <td className="pl-4">{index + 1}</td>
                           <td>
                              <h5 className="font-medium mb-0"> {classe.classeEleve}</h5>
                           </td>
                           <td>
                              <button onClick={() => deleteClasse(classe)} type="button" className="btn btn-outline-info ml-2"><DeleteForeverIcon/></button>
                              <Link to={"/classe/" + classe._id} >
                                 <button type="button" className="btn btn-outline-info ml-2"><EditIcon /></button>
                              </Link>
                              </td>
                        </tr>
                     ))}
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   )
};


export default GestionClasses;
