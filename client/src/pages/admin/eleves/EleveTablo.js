import React from 'react'
import RoutesEleves from "../../../components/routes/RoutesEleves";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import EditIcon from '@material-ui/icons/Edit';
import { LinearProgress, makeStyles, Modal } from '@material-ui/core';
import FeaturedPlayListIcon from '@material-ui/icons/FeaturedPlayList';
import CarteEleve from "./CardEleve"

function rand() {
   return Math.round(Math.random() * 20) - 10;
 }
 
 function getModalStyle() {
   const top = 50 + rand();
   const left = 50 + rand();
 
   return {
     top: `${top}%`,
     left: `${left}%`,
     transform: `translate(-${top}%, -${left}%)`,
   };
 }

 const useStyles = makeStyles((theme) => ({
   paper: {
     position: 'absolute',
     maxWidth: 800,
     backgroundColor: theme.palette.background.paper,
     border: '2px solid #000',
     boxShadow: theme.shadows[5],
     padding: theme.spacing(2, 4, 3),
   },
 }));



export default function EleveTablo ({eleves, retrieveEleves, eleClasses}){
   
   const classes = useStyles();

   //States
   const [progress, setProgress] = React.useState(0);
   const [selectedEleve, setSelectedEleve] = React.useState();
   const [modalStyle] = React.useState(getModalStyle);
   const [open, setOpen] = React.useState(false);

   React.useEffect(() => {
      //Fake barre de progression
      const timer = setInterval(() => {
        setProgress((oldProgress) => {
          if (oldProgress === 100) {
            return 0;
          }
          const diff = Math.random() * 10;
          return Math.min(oldProgress + diff, 100);
        });
      }, 500);
  
      return () => {
        clearInterval(timer);
      };
    }, []);

   //Suppression d'un élève
   const deleteEleve = (eleve) => {
      RoutesEleves.remove(eleve._id)
      .then(response => {
         console.log(response.data);
         retrieveEleves()
      })
      .catch(e => {
         console.log(e);
      });
   };

   const editEleve = (eleve) => {
      setSelectedEleve(eleve._id)
      handleOpen();
   }

   const carteEleve = (eleve) => {
      setSelectedEleve(eleve._id)
      handleOpen();
   }

   const handleOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };

    const body = (
      <div style={modalStyle} className={classes.paper}>
        <CarteEleve props={selectedEleve} />
      </div>
    );

   //Quand ça charge...
   while (eleves.length <= 0) {
      return <LinearProgress variant="determinate" value={progress} />
   }
   //Chargement fini, affichage:
   return (
      
      <div className="row">
         <div className="col-md-12">
            <div className="card">
               <div className="card-body">
                  <h5 className="card-title text-uppercase mb-0">Élèves</h5>
               </div>
            <div className="table-responsive">
            <table className="table no-wrap user-table mb-0">
               <thead>
                  <tr>
                     <th scope="col" className="border-0 text-uppercase font-medium">#</th>
                     <th scope="col" className="border-0 text-uppercase font-medium">Nom</th>
                     <th scope="col" className="border-0 text-uppercase font-medium">Classe</th>
                     <th scope="col" className="border-0 text-uppercase font-medium">Contact</th>
                     <th scope="col" className="border-0 text-uppercase font-medium">Date de naissance</th>
                     <th scope="col" className="border-0 text-uppercase font-medium">Manage</th>
                  </tr>
               </thead>
               <tbody>
                  {eleves.map((eleve, index) => (
                     <tr key={eleve._id}>
                        <td className="pl-4">{index + 1}</td>
                        <td>
                           <h5 className="font-medium mb-0"> {eleve.eleNom}</h5>
                           <span className="text-muted">{eleve.elePrenom}</span>
                        </td>
                        <td>
                  <span className="text-muted">{eleClasses.find(classe => classe._id === eleve.eleClasse).classeEleve}</span>
                        </td>
                        <td>
                           <span className="text-muted">{eleve.eleTel}</span><br />
                           <span className="text-muted">{eleve.eleTelSec}</span>
                        </td>
                        <td>
                           <span className="text-muted">{eleve.eleBirthday}</span>
                        </td>
                        <td>
                           <button onClick={() => carteEleve(eleve)} type="button" className="btn btn-outline-info ml-2"><FeaturedPlayListIcon/></button>
                           <button onClick={() => deleteEleve(eleve)} type="button" className="btn btn-outline-info ml-2"><DeleteForeverIcon/></button>
                           <button onClick={() => editEleve(eleve)} type="button" className="btn btn-outline-info ml-2"><EditIcon /></button>
                           <Modal
                           open={open}
                           onClose={handleClose}
                           aria-labelledby="simple-modal-title"
                           aria-describedby="simple-modal-description"
                           >
                           {body}
                           </Modal>
                           </td>
                     </tr>
                  ))}
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>
)
}
