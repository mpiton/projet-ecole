import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import RoutesEleves from "../../../components/routes/RoutesEleves";
import { FormControl, InputLabel, Select } from '@material-ui/core';


const useStyles = makeStyles((theme) => ({
   td: {
      border: "1px solid #726E6D",
      padding:"15px"
   },
   thead: {
      fontWeight:"bold",
      textAlign:"center",
      background:"#625D5D",
      color:"white"
   },
   table: {
      borderCollapse: "collapse",
      margin: "0 auto"
   },
   footer: {
      textAlign:"right",
      fontWeight:"bold",
      border: "1px solid #726E6D",
      padding:"15px"
   },
   formControl: {
      marginLeft: "40%",
      marginBottom: "20px",
      minWidth: 120,
    },
}));

const Note = () => {
   const classes = useStyles();

   const [eleves, setEleves] = useState();
   const [selected, setSelected] = useState();

   useEffect(() => {
      //Récupération de tous les élèves
      const retrieveEleves = () => {
         RoutesEleves.getAll()
         .then(response => {
            setEleves(response.data.eleves);
            console.log(eleves)
         })
         .catch(e => {
         console.log(e);
         });
      };
      retrieveEleves();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

   const handleChange = (event) => {
      setSelected(event.target.value);
    };

   return (
      <React.Fragment>
         {eleves ? (
            <FormControl variant="outlined" className={classes.formControl}>
               <InputLabel>Elève</InputLabel>
               <Select
               native
               value={selected}
               onChange={handleChange}
               >
                  <option aria-label="choisir le nom de l'élève" value="" />
                  {eleves.map((eleve, index) => {
                     return <option aria-label={eleve.eleNom + " " + eleve.elePrenom} key={index} value={eleve.eleNom}>{eleve.eleNom + " " + eleve.elePrenom}</option>
                  })}
               </Select>
            </FormControl>
      ) : null}
      <div className="table-responsive">
         <table className={classes.table}>
            <thead className={classes.thead} >
               <tr>
                  <td className={classes.td} colSpan="3">Matières</td>
                     <td className={classes.td}  rowSpan="2">Timestre</td>
                     <td className={classes.td} rowSpan="2">Coéficients</td>
                     <td className={classes.td} colSpan="2">Notes</td>
               </tr>
               <tr>
                     <td className={classes.td}>Nom</td>
                     <td className={classes.td} colSpan="2">Sujet</td>
                     <td className={classes.td} > Lettre</td>
                     <td className={classes.td} > Points </td>
               </tr>
            </thead>
            <tbody>
               <tr style={{backgroundColor:'#D1D0CE'}}>
                  <td className={classes.td}  >Histoire </td>
                  <td className={classes.td} colSpan="2" >1ère Guerre Mondiale </td>
                  <td className={classes.td} > 2ème Trimestre 2021</td>
                  <td className={classes.td} > 3.0 </td>
                  <td className={classes.td} > A </td>
                  <td className={classes.td} > 16.5 </td>
               </tr>
               <tr>
                  <td className={classes.td} >Mathématique </td>
                  <td className={classes.td} colSpan="2">Théorème de Pythagore </td>
                  <td className={classes.td} > 2ème Trimestre 2021</td>
                  <td className={classes.td} > 2.0 </td>
                  <td className={classes.td} > A- </td>
                  <td className={classes.td} > 11 </td>
               </tr>
               <tr style={{backgroundColor:'#D1D0CE'}}>
                  <td className={classes.td} >Science </td>
                  <td className={classes.td} colSpan="2">Dioxygène </td>
                  <td className={classes.td} > 2ème Trimestre 2021</td>
                  <td className={classes.td} > 2.0 </td>
                  <td className={classes.td} > A </td>
                  <td className={classes.td} > 12 </td>
               </tr>
               <tr>
                  <td className={classes.td} >Sport </td>
                  <td className={classes.td} colSpan="2">Endurance </td>
                  <td className={classes.td} > 2ème Trimestre 2021</td>
                  <td className={classes.td} > 3.0 </td>
                  <td className={classes.td} > B+ </td>
                  <td className={classes.td} > 10</td>
               </tr>
               <tr style={{backgroundColor:'#D1D0CE'}}>
                  <td className={classes.td} >Français </td>
                  <td className={classes.td} colSpan="2">Texte Littéraire: Albert Camus </td>
                  <td className={classes.td} >2ème Trimestre 2021</td>
                  <td className={classes.td} > 3.0 </td>
                  <td className={classes.td}  > A- </td>
                  <td className={classes.td} > 11 </td>
               </tr >
            </tbody>
            <tfoot>
               <tr>
                  <td className={classes.footer} colSpan="4" >Total</td>
                  <td className={classes.td} > 12.1 </td>
                  <td className={classes.td} colSpan="2">60.5 </td>
               </tr>
            </tfoot>
         </table>
      </div>
   </React.Fragment>
   )
}

export default Note
