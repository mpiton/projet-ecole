import React, { useState, useEffect } from "react";
import RoutesEleves from "../../../components/routes/RoutesEleves";
import { Avatar, List, ListItem, ListItemAvatar, ListItemText, makeStyles } from "@material-ui/core";
import RoutesClasses from "../../../components/routes/RoutesClasses";
import RoutesMatieres from "../../../components/routes/RoutesMatieres";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import clsx from 'clsx';
import ImageIcon from '@material-ui/icons/Image';
import WorkIcon from '@material-ui/icons/Work';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';

//Style
const useStyles = makeStyles((theme) => ({
   root: {
       expand: {
         transform: 'rotate(0deg)',
         marginLeft: 'auto',
         transition: theme.transitions.create('transform', {
           duration: theme.transitions.duration.shortest,
         }),
       },
       expandOpen: {
         transform: 'rotate(180deg)',
       },
       title: {
         fontSize: 14,
       },
   },
}));

const CardEleve = props => {



   useEffect(() => {
   //Récupération de l'id en string
   const maProps = props.props
   RoutesMatieres.getAll()
      .then(response => {
         setMatieres(response.data.matieres);
         //console.log(matieres);
      })
      .catch(e => {
         console.log(e);
      });
   RoutesClasses.getAll()
      .then(response => {
         setClasse([response.data.classes]);
      })
      .catch(e => {
         console.log(e);
      });
   RoutesEleves.get(maProps)
      .then(response => {
         setCurrentEleve(response.data.eleve);
      })
      .catch(e => {
      console.log(e);
   });
   }, [props.props]);

   //déclaration du style
   const classesStyle = useStyles();



   //STATES
   const [currentEleve, setCurrentEleve] = useState({});
   const [classes, setClasse] = useState([]);
   const [matieres, setMatieres] = useState([]);
   const [expanded, setExpanded] = React.useState(false);

   //Expend l'affichage
   const handleExpandClick = () => {
     setExpanded(!expanded);
   };

   

  //Affichage
   return (
      
      <Card className={classesStyle.root}>
      {currentEleve ? (
         <React.Fragment>
      <CardContent>
         <Typography className={classesStyle.title} color="textSecondary" gutterBottom>
            {currentEleve.eleNom + " " + currentEleve.elePrenom}
         </Typography>
         <List className={classesStyle.root}>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <ImageIcon />
          </Avatar>
        </ListItemAvatar>
        {/* <ListItemText primary="Classe" secondary={classes.find(classe => classe._id === currentEleve.eleClasse)} /> A REVOIR*/}
      </ListItem>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <ImageIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Téléphone" secondary={currentEleve.eleTel} />
      </ListItem>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <WorkIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Téléphone Secondaire" secondary={currentEleve.eleTelSec} />
      </ListItem>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <BeachAccessIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Date de naissance" secondary={currentEleve.eleBirthday} />
      </ListItem>
    </List>
      </CardContent>
      <CardActions disableSpacing>
        <Typography>En savoir plus</Typography>
        <IconButton
          className={clsx(classesStyle.expand, {
            [classesStyle.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>Method:</Typography>
          <Typography paragraph>
            Heat 1/2 cup of the broth in a pot until simmering, add saffron and set aside for 10
            minutes.
          </Typography>
          <Typography paragraph>
            Heat oil in a (14- to 16-inch) paella pan or a large, deep skillet over medium-high
            heat. Add chicken, shrimp and chorizo, and cook, stirring occasionally until lightly
            browned, 6 to 8 minutes. Transfer shrimp to a large plate and set aside, leaving chicken
            and chorizo in the pan. Add pimentón, bay leaves, garlic, tomatoes, onion, salt and
            pepper, and cook, stirring often until thickened and fragrant, about 10 minutes. Add
            saffron broth and remaining 4 1/2 cups chicken broth; bring to a boil.
          </Typography>
          <Typography paragraph>
            Add rice and stir very gently to distribute. Top with artichokes and peppers, and cook
            without stirring, until most of the liquid is absorbed, 15 to 18 minutes. Reduce heat to
            medium-low, add reserved shrimp and mussels, tucking them down into the rice, and cook
            again without stirring, until mussels have opened and rice is just tender, 5 to 7
            minutes more. (Discard any mussels that don’t open.)
          </Typography>
          <Typography>
            Set aside off of the heat to let rest for 10 minutes, and then serve.
          </Typography>
        </CardContent>
      </Collapse>
      </React.Fragment>
      ) : (
       <div>
         <h4>Erreur de récupération de l'élève</h4>
       </div>
    )}
    </Card>

  );
};

export default CardEleve;