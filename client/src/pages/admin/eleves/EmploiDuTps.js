import { FormControl, InputLabel, makeStyles, Select, Typography } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import "../../../css/EmploiDuTps.css"
import RoutesProfs from "../../../components/routes/RoutesProfs";

const useStyles = makeStyles((theme) => ({
   formControl: {
      minWidth: 300,
    },
    titre: {
       marginTop: theme.spacing(3),
      marginBottom: "1em"
    }
}));

const EmploiDuTps = () => {
   const classes = useStyles();

   const [professeurs, setProfesseurs] = useState();
   const [selected, setSelected] = useState();

   useEffect(() => {
   //Récupération de tous les professeurs
   const retrieveProf = () => {
      RoutesProfs.getAll()
      .then(response => {
         setProfesseurs(response.data.profs);
      })
      .catch(e => {
         console.log(e);
      });
   };
      retrieveProf();
    }, []);

   const handleChange = (event) => {
      setSelected(event.target.value);
    };
      return (
         <div className="container">
  <div className="timetable-img text-center">
  {professeurs ? (
            <FormControl variant="outlined" className={classes.formControl}>
               <InputLabel>Professeurs</InputLabel>
               <Select
               native
               value={selected}
               onChange={handleChange}
               >
                  <option aria-label="choisir le nom du professeur" value="" />
                  {professeurs.map((professeur, index) => {
                     return <option aria-label={professeur.profNom + " " + professeur.profPrenom} key={index} value={professeur.profNom}>{professeur.profNom + " " + professeur.profPrenom}</option>
                  })}
               </Select>
            </FormControl>
      ) : null}
  <Typography variant="h5" color="textSecondary" className={classes.titre}>Emploi du temps</Typography>
  </div>
  <div className="table-responsive overflow-auto">
    <table className="table table-bordered text-center">
      <thead>
        <tr className="bg-light-gray">
          <th className="text-uppercase">Heure
          </th>
          <th className="text-uppercase">Lundi</th>
          <th className="text-uppercase">Mardi</th>
          <th className="text-uppercase">Mercredi</th>
          <th className="text-uppercase">Jeudi</th>
          <th className="text-uppercase">Vendredi</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td className="align-middle">09:00</td>
          <td>
            <span className="bg-sky padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16 xs-font-size13">4ème C</span>
            <div className="margin-10px-top font-size14">9:00-10:00</div>
            <div className="font-size13">Salle 104</div>
          </td>
          <td>
            <span className="bg-green padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">5ème B</span>
            <div className="margin-10px-top font-size14">9:00-10:00</div>
            <div className="font-size13">Salle 105</div>
          </td>
          <td>
            <span className="bg-yellow padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">6ème C</span>
            <div className="margin-10px-top font-size14">9:00-10:00</div>
            <div className="font-size13">Salle 104</div>
          </td>
          <td>
          <span className="bg-purple padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">3ème B</span>
            <div className="margin-10px-top font-size14">9:00-10:00</div>
            <div className="font-size13">Salle 105</div>
          </td>
          <td>
            <span className="bg-purple padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">3ème A</span>
            <div className="margin-10px-top font-size14">9:00-10:00</div>
            <div className="font-size13">Salle 105</div>
          </td>
        </tr>
        <tr>
          <td className="align-middle">10:00</td>
          <td>
          <span className="bg-green padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">5ème C</span>
            <div className="margin-10px-top font-size14">10:00-11:00</div>
            <div className="font-size13">Salle 104</div>
          </td>
          <td className="bg-light-gray">
          </td>
          <td>
          <span className="bg-purple padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">3ème C</span>
            <div className="margin-10px-top font-size14">10:00-11:00</div>
            <div className="font-size13">Salle 104</div>
          </td>
          <td>
          <span className="bg-green padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">5ème A</span>
            <div className="margin-10px-top font-size14">10:00-11:00</div>
            <div className="font-size13">Salle 104</div>
          </td>
          <td className="bg-light-gray">
          </td>
        </tr>
        <tr>
          <td className="align-middle">11:00</td>
          <td>
          <span className="bg-sky padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16 xs-font-size13">4ème B</span>
            <div className="margin-10px-top font-size14">11:00-12:00</div>
            <div className="font-size13">Salle 105</div>
          </td>
          <td>
          <span className="bg-purple padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">3ème A</span>
            <div className="margin-10px-top font-size14">11:00-12:00</div>
            <div className="font-size13">Salle 105</div>
          </td>
          <td>
          <span className="bg-purple padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">3ème C</span>
            <div className="margin-10px-top font-size14">11:00-12:00</div>
            <div className="font-size13">Salle 105</div>
          </td>
          <td className="bg-light-gray">
          </td>
          <td className="bg-light-gray">
          </td>
        </tr>
        <tr>
          <td className="align-middle">12:00</td>
          <td>
          <span className="bg-lightred padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">Pause repas</span>
            <div className="margin-10px-top font-size14">12:00-14:00</div>
          </td>
          <td>
          <span className="bg-lightred padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">Pause repas</span>
            <div className="margin-10px-top font-size14">12:00-14:00</div>
          </td>
          <td>
          <span className="bg-lightred padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">Pause repas</span>
            <div className="margin-10px-top font-size14">12:00-14:00</div>
          </td>
          <td>
          <span className="bg-lightred padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">Pause repas</span>
            <div className="margin-10px-top font-size14">12:00-14:00</div>
          </td>
          <td>
          <span className="bg-lightred padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">Pause repas</span>
            <div className="margin-10px-top font-size14">12:00-14:00</div>
          </td>
        </tr>
        <tr>
          <td className="align-middle">14:00</td>
          <td>
          <span className="bg-yellow padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">6ème B</span>
            <div className="margin-10px-top font-size14">14:00-15:00</div>
            <div className="font-size13">Salle 105</div>
          </td>
          <td>
          <span className="bg-sky padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16 xs-font-size13">4ème A</span>
            <div className="margin-10px-top font-size14">14:00-15:00</div>
            <div className="font-size13">Salle 105</div>
          </td>
          <td className="bg-light-gray">
          </td>
          <td className="bg-light-gray">
          </td>
          <td>
          <span className="bg-yellow padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">6ème A</span>
            <div className="margin-10px-top font-size14">14:00-15:00</div>
            <div className="font-size13">Salle 104</div>
          </td>
        </tr>
        <tr>
          <td className="align-middle">15:00</td>
          <td className="bg-light-gray">
          </td>
          <td>
          <span className="bg-purple padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">3ème B</span>
            <div className="margin-10px-top font-size14">15:00-16:00</div>
            <div className="font-size13">Salle 105</div>
          </td>
          <td className="bg-light-gray">
          </td>
          <td>
          <span className="bg-purple padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">3ème C</span>
            <div className="margin-10px-top font-size14">15:00-16:00</div>
            <div className="font-size13">Salle 104</div>
          </td>
          <td className="bg-light-gray">
          </td>
        </tr>
        <tr>
          <td className="align-middle">16:00</td>
          <td>
          <span className="bg-green padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">5ème A</span>
            <div className="margin-10px-top font-size14">16:00-17:00</div>
            <div className="font-size13">Salle 105</div>
          </td>
          <td className="bg-light-gray">
          </td>
          <td className="bg-light-gray">
          </td>
          <td>
          <span className="bg-sky padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16 xs-font-size13">4ème A</span>
            <div className="margin-10px-top font-size14">16:00-17:00</div>
            <div className="font-size13">Salle 104</div>
          </td>
          <td>
          <span className="bg-yellow padding-5px-tb padding-15px-lr border-radius-5 margin-10px-bottom font-weight-bold font-size16  xs-font-size13">6ème B</span>
            <div className="margin-10px-top font-size14">16:00-17:00</div>
            <div className="font-size13">Salle 104</div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

      )
   }

export default EmploiDuTps
