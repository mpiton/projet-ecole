import React, { useState, useEffect } from "react";
import RoutesClasses from "../../../components/routes/RoutesClasses";
import RoutesEleves from "../../../components/routes/RoutesEleves";
import '../../../css/ProfListe.css'
import EleveTablo from "../eleves/EleveTablo"
import Pagination from "../eleves/Pagination"

const ListeEleves = () => {
  const [eleves, setEleves] = useState([]);
  const [searchNom, setSearchNom] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [eleveParPage] = useState(10)
  const [totalEleves, setTotalEleves] = useState("")
  const [eleClasses, setEleClasses] = useState([])


  useEffect(() => {
     retrieveClasses();
   return retrieveEleves();
 }, []);

    //Récupération de toutes les classes
    const retrieveClasses = () => {
      RoutesClasses.getAll()
      .then(response => {
         setEleClasses(response.data.classes);
      })
      .catch(e => {
         console.log(e);
      });
   };

   //Récupération de tous les élèves
   const retrieveEleves = () => {
   RoutesEleves.getAll()
            .then(response => {
               setEleves(response.data.eleves);
               setTotalEleves(response.data.total)
            })
            .catch(e => {
               console.log(e);
            });
         };
  //Récupération de la valeur dans la barre search
  const onChangeSearchNom = e => {
    const searchNom = e.target.value;
    setSearchNom(searchNom);
  };

  //Modifie la valeur de la pagination
  const handleChange = (event, value) => {
   setCurrentPage(value);
 };

   //Recherche par nom
   const findByNom= () => {
   RoutesEleves.findByNom(searchNom)
      .then(response => {
      setEleves(response.data.eleve);
      console.log(response.data.eleve);
      })
      .catch(e => {
      console.log(e);
      });
};
//Récupération des profs actuellement affichés
const indexOfLastEleve = currentPage * eleveParPage;
const indexOfFirstEleve = indexOfLastEleve - eleveParPage;
const currentEleve = eleves.slice(indexOfFirstEleve, indexOfLastEleve)
//Méthode changement de page
const paginate = (nb) => setCurrentPage(nb)


return (
   <div className="">
      <div className="list row">
         <div className="col-md-8">
            <div className="input-group mb-3">
               <input
               type="text"
               className="form-control"
               placeholder="Rechercher par nom"
               value={searchNom}
               onChange={onChangeSearchNom}
               />
            <div className="input-group-append">
            <button
            className="btn btn-outline-secondary"
            type="button"
            onClick={findByNom}
            >
            Rechercher
            </button>
            </div>
         </div>
      </div>
   </div>
   <EleveTablo retrieveEleves={retrieveEleves} eleves={currentEleve} eleClasses={eleClasses} />
   <Pagination change={handleChange} paginate={paginate} eleveParPage={eleveParPage} totalEleves={totalEleves} />
   </div>
);
};


export default ListeEleves;
