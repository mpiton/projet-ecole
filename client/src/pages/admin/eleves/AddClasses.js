import React, { useState } from "react";
import RoutesClasses from "../../../components/routes/RoutesClasses";
import {
   Input, 
   FormControl,
   InputLabel,
   Button,
   FormHelperText,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
   root: {
     '& > *': {
       margin: theme.spacing(1),
     },
   },
   formControl: {
      margin: theme.spacing(1),
      minWidth: 300,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    error: {
      color: "crimson",
      fontSize: "bold"
   }
 }));

const AddClasses = () => {
   //STATE Initial
  const initialClassesState = {
    _id: null,
    classeEleve: ""
  };

  //STATES
  const [classe, setClasses] = useState(initialClassesState);
  const [submitted, setSubmitted] = useState(false);
  const [errorMessage, setErrorMessage] = useState("")

  //Recupére les infos au changement
  const handleInputChange = event => {
    const { name, value } = event.target;
    setClasses({ ...classe, [name]: value });
  };

  // Fonction de save en BDD
  const saveClasse = () => {
   /// VALIDATION
      // Si le champs est rempli
    if (classe.classeEleve) {
      const pattern = /^[a-zA-Zéè0-9_-\s]{3,6}$/;
      const result = pattern.test(classe.classeEleve);
      //Si le test passe
      if (result === true) {
              //Initialisation des champs
               var data = {
                  classeEleve: classe.classeEleve
               };
      }
      // Sinon envoie du message d'erreur
      else {
         return setErrorMessage(`${classe.classeEleve} n'est pas un nom de matière valide`)
      }
        //Ajout de la donnée reçu dans les bon champs
    RoutesClasses.create(data)
      .then(response => {
        setClasses({
          id: response.data._id,
          classeEleve: response.data.classeEleve
        });
        setSubmitted(true);
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      })
   }
// Si le champs n'est pas rempli
   else {
         return setErrorMessage("Veuillez remplir le champs obligatoire")
     };
}

  const newClasse = () => {
    setClasses(initialClassesState);
    setSubmitted(false);
  };

  const classesStyle = useStyles();

  return (
   <div className="submit-form">
   {submitted ? (
     <div>
       <h4>La classe a été ajouté!</h4>
       <Button variant="contained" color="primary" onClick={newClasse}>
        Ajouter une autre
      </Button>
     </div>
   ) : (
     <div>
         <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="classeEleve">Nom de la classe</InputLabel>
            <Input id="classeEleve" required={true} value={classe.classeEleve} onChange={handleInputChange} name="classeEleve" placeholder="Nom de la classe" inputProps={{ 'aria-label': 'nom de la classe' }} />
            <FormHelperText className={classesStyle.error}>{errorMessage}</FormHelperText>
         </FormControl>

      <div className={classesStyle.root}>
      <Button variant="contained" color="primary" onClick={saveClasse}>
        Ajouter
      </Button>
    </div>
     </div>
   )}

 </div>
);
};

export default AddClasses;
