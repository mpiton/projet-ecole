import React, { useState, useEffect } from 'react'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from "@fullcalendar/interaction"
import RoutesReunions from "../../../components/routes/RoutesReunion";
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import AddReunion from "./AddReunion"
import EditReunion from "./EditReunion"
import frLocale from "date-fns/locale/fr"
import RoutesReunion from "../../../components/routes/RoutesReunion"
import { Button } from '@material-ui/core';

//Style
const useStyles = makeStyles((theme) => ({
   paper: {
     position: 'absolute',
     width: 400,
     backgroundColor: theme.palette.background.paper,
     border: '2px solid #000',
     boxShadow: theme.shadows[5],
     padding: theme.spacing(2, 4, 3),
   },
 }));

const Reunions = () => { 
   //STATES
   const [reunions, setReunions] = useState([]);
   const [modalStyle] = React.useState(getModalStyle);
   const [open, setOpen] = React.useState(false);
   const [edit, setEdit] = React.useState(false);
   const [findById, setFindById] = React.useState("")

   useEffect(() => {
      retrieveReunions();
    }, []);


    //Style Modal
  function rand() {
   return Math.round(Math.random() * 20) - 10;
 }
 
 //Position de la modal
 function getModalStyle() {
   const top = 50 + rand();
   const left = 50 + rand();
 
   return {
     top: `${top}%`,
     left: `${left}%`,
     transform: `translate(-${top}%, -${left}%)`,
   };
 }

    // GET ALL Reunions
    const retrieveReunions = () => {
      RoutesReunions.getAll()
      .then(response => {
         //parse le format date
         const reunion = response.data.reunions
         reunion.forEach(reunion => {
            var dateObj = new Date(reunion.date);
            var month = dateObj.getUTCMonth() + 1;
            var day = dateObj.toLocaleDateString("fr-FR").slice(0,2)
            var year = dateObj.getUTCFullYear();
            reunion.date = year + "-" + month + "-" + day;
            console.log(reunion.date)
         });
         setReunions(response.data.reunions);
       })
       .catch(e => {
         console.log(e);
       });
    };

    const classesStyle = useStyles();

     //Ouverture de la modal
  const handleOpen = () => {
   setOpen(true);
 };

 //Fermeture de la modal
 const handleClose = () => {
   setOpen(false);
   setEdit(false);
   retrieveReunions();
 };

 //Contenu de la modal d'ajout
 const add = (
   <div style={modalStyle} className={classesStyle.paper}>
     <h3 id="simple-modal-title">Ajoutez une réunion</h3>
     <AddReunion />
   </div>
 );
  //Contenu de la modal d'edit
 const editer = (
   <div style={modalStyle} className={classesStyle.paper}>
     <h3 id="simple-modal-title">Editer une réunion</h3>
     <EditReunion id={findById} />
   </div>
 ); 

 //Ondrop on save dans la db
 const onDrop = (args) => {
    console.log(args);
   var data = {
      title: args.event._def.title,
      date: args.event._instance.range.start,
   };
   console.log(args.event._def.extendedProps._id);
      RoutesReunion.update(args.event._def.extendedProps._id, data)
      .then(response => {
         console.log(response.data);
      })
      .catch(e => {
         console.log(e);
      });
 }

 //Click on date
 const editReunion = (arg) => { 
    setFindById(arg.event._def.extendedProps._id);
    console.log(arg.event._def.extendedProps._id);
    setEdit(true);
    setOpen(true);
 }
   return (
      <div>
         <div>
      <Button variant="contained" color="primary" onClick={handleOpen}>Ajouter une réunion</Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {edit ? editer : add}
      </Modal>
    </div>
      <FullCalendar
        initialView="dayGridMonth"
        weekends={false}
        editable={true}
        plugins={[ dayGridPlugin, interactionPlugin ]}
        dateClick={handleOpen}
        events={reunions}
        eventClick={editReunion}
        nowIndicator={true}
        locale={frLocale}
        eventDrop={onDrop}
        
      />
      </div>
   )
}

export default Reunions
