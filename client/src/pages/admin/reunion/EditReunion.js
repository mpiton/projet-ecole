import React, { useState, useEffect } from "react";
import { Button, FormControl, Input, makeStyles} from "@material-ui/core";
import RoutesReunion from "../../../components/routes/RoutesReunion"

//Style
const useStyles = makeStyles((theme) => ({
   root: {
     '& .MuiTextField-root': {
       margin: theme.spacing(1),
       width: '25ch',
     },
   },
   formControl: {
      margin: theme.spacing(1),
      minWidth: 300,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    delete: {
       backgroundColor: "crimson",
       marginRight: "20px",
    },
    bouton: {
       marginTop: theme.spacing(1),
       marginBottom: "30px",
       display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'center'
    },
    error: {
      color: "crimson",
      fontSize: "bold"
   }
 }));
  

const Reunion = props => {


   useEffect(() => {
      return getReunion(props.id);
   },[props.id]);

   //déclaration du style
   const classesStyle = useStyles();

   //Récupération de l'id
   // const id = props

   //STATES
   const [reunion, setReunion] = useState({})
   const [message, setMessage] = useState("")

   //Méthode de récupération du professeur
   const getReunion = id => {
      RoutesReunion.get(id)
      .then(response => {
         console.log(response.data.reunion.date);
         //parse la date
         // var date = new Date(response.data.reunion.date);
         // var min_date = date.toISOString().slice(0,10);
         var dateObj = new Date(response.data.reunion.date);
         var month = dateObj.getUTCMonth() + 1;
         var day = dateObj.toLocaleDateString("fr-FR").slice(0,2)
         var year = dateObj.getUTCFullYear();
         var min_date = year + "-" + month + "-" + day;
         response.data.reunion.date = min_date
         setReunion(response.data.reunion);
      })
      .catch(e => {
         console.log(e);
      });
   };

   //écouteur sur les inputs
   const handleInputChange = event => {
      const { name, value } = event.target;
      setReunion({ ...reunion, [name]: value });
   };


   //méthode de mise à jour de la réunion
   const updateReunion = () => {
      const pattern = /^[a-zA-Z0-9_ 'àçéèêë()+-]*$/;
      const result = pattern.test(reunion.title)
      // Si le champs titre est correct
      if (result === true) {
         const pattern = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
         const result = pattern.test(reunion.date)
         //Si le champ date est correct
         if (result === true) {
            //Initialisation des champs
            var data = {
               title: reunion.title,
               date: reunion.date
            };
               RoutesReunion.update(reunion._id, data)
               .then(response => {
                  console.log(response.data);
                  setMessage("Les informations ont été mise à jour");
               })
               .catch(e => {
                  console.log(e);
               });
         } else {
            return setMessage("Format date non valide")
         }
      } else {
         return setMessage("Le champs saisi n'est pas valide")
      }
   };

   //méthode de suppression du professeur
   const deleteReunion = () => {
      RoutesReunion.remove(reunion._id)
      .then(response => {
         console.log(response.data);
         return setMessage("Réunion supprimé avec succès")
      })
      .catch(e => {
        console.log(e);
      });
  };

  //Affichage
  return (
   <div>
     <div className="edit-form">
       <form className={classesStyle.root} noValidate autoComplete="off">
      <div>
        <FormControl margin="normal" fullWidth>
           
            <Input id="titre" required={true} value={reunion.title} onChange={handleInputChange} name="title" placeholder="Titre" inputProps={{ 'aria-label': 'Titre de la réunion' }} />
         </FormControl>

         <FormControl margin="normal" fullWidth>
          
            <Input type="date" id="date" required={true} value={reunion.date} onChange={handleInputChange} name="date" placeholder="Date" inputProps={{ 'aria-label': 'Date de la réunion' }} />
         </FormControl>




         </div>
       </form>
   
         <div className={classesStyle.bouton}>
          <Button variant="contained" onClick={deleteReunion} className={classesStyle.delete}>
  Supprimer
</Button>
<Button type="submit" variant="contained" color="primary" onClick={updateReunion}>
  Mettre à jour
</Button>
</div>
<p className={classesStyle.error}>{message}</p>
        </div>
    </div>
  );
};

export default Reunion;
