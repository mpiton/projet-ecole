import React, { useState, useEffect } from "react";
import RoutesReunions from "../../../components/routes/RoutesReunion";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import {
   Input,
   FormControl,
   InputLabel,
   Button,
   TextField,
   FormHelperText
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

//Message sucess
function Alert(props) {
   return <MuiAlert elevation={6} variant="filled" {...props} />;
 }

 //Style
const useStyles = makeStyles((theme) => ({
   root: {
     '& > *': {
       margin: theme.spacing(1),
     },
   },
   formControl: {
      margin: theme.spacing(1),
      minWidth: 300,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    error: {
      color: "crimson",
      fontSize: "bold"
   }
 }));



const AddReunion = () => {
   //STATE Initial
  const initialReunionState = {
    _id: null,
    title: "",
    date: ""
  };

  //STATES
  const [reunion, setReunion] = useState(initialReunionState);
  const [submitted, setSubmitted] = useState(false);
  const [errorMessage, setErrorMessage] = useState("")
  const [open, setOpen] = React.useState(false);

  //Recupére les infos au changement
  const handleInputChange = event => {
    const { name, value } = event.target;
    setReunion({ ...reunion, [name]: value });
  };


  useEffect(() => {
   retrieveReunions();
 }, []);

      //Récupération de toutes les classes
      const retrieveReunions = () => {
         RoutesReunions.getAll()
         .then(response => {
            setReunion(response.data.reunions);
         })
         .catch(e => {
            console.log(e);
         });
      };


  // Fonction de save en BDD
  const saveReunion = () => {
      if (reunion.title && reunion.date) {
      const pattern = /^[a-zA-Z0-9_ 'àçéèêë()+-]*$/;
      const result = pattern.test(reunion.title);
      if (result === true) {
              // Si le champs titre est correct
         const pattern = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
         const result = pattern.test(reunion.date)
         //Si le champ date est correct
         if (result === true) {
            //Initialisation des champs
         //Initialisation des champs
         var data = {
         title: reunion.title,
         date: reunion.date
         };
      }
      else {
         return setErrorMessage("Titre de la réunion incorrect")
      }
   //Ajout de la donnée reçu dans les bon champs
   RoutesReunions.create(data)
      .then(response => {
         setReunion({
         id: response.data._id,
         title: response.data.title,
         date: response.data.date
         });
      setSubmitted(true);
      console.log(response.data);
      })
      .catch(e => {
         console.log(e);
      });
   }
   else {
      return setErrorMessage("Veuillez remplir tous les champs")
   }
 }
}


 const handleClose = (event, reason) => {
   if (reason === 'clickaway') {
     return;
   }
   setOpen(false);
 };

  const newReunion = () => {
    setReunion(initialReunionState);
    setSubmitted(false);
  };

  const classesStyle = useStyles();

  return (
   <div className="submit-form">
   {submitted ? (
     <div>
       <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success">
           Réunion ajoutée avec succès!
        </Alert>
      </Snackbar>
      <Alert severity="success">Réunion ajoutée avec succès!</Alert>
       <Button variant="contained" color="primary" onClick={newReunion}>
        Ajouter une autre
      </Button>
     </div>
   ) : (
     <div>
         <FormControl className={classesStyle.formControl}>
            <InputLabel htmlFor="title">Titre de la réunion</InputLabel>
            <Input id="title" required={true} value={reunion.title} onChange={handleInputChange} name="title" placeholder="Titre" inputProps={{ 'aria-label': 'titre de la réunion' }} />
            <FormHelperText className={classesStyle.error}>{errorMessage}</FormHelperText>
            </FormControl>

      <FormControl className={classesStyle.formControl}>
      <TextField
        id="date"
        required={true}
        label="Date de la réunion"
        type="date"
        className={classesStyle.textField}
        InputLabelProps={{
          shrink: true,
        }}
        value={reunion.date}
        onChange={handleInputChange}
        name="date"
      />
      </FormControl>

      <div className={classesStyle.root}>
      <Button variant="contained" color="primary" onClick={saveReunion}>
        Ajouter
      </Button>
    </div>
     </div>
   )}

 </div>
);
};

export default AddReunion;
