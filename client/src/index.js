import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import * as serviceWorker from './serviceWorker';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import steelblue from '@material-ui/core/colors/blue';
import dodgerblue from '@material-ui/core/colors/blue';
import { BrowserRouter } from "react-router-dom";
import './index.css';


//Material UI https://material-ui.com/customization/palette/
const theme = createMuiTheme({
   palette: {
      primary:  steelblue,
      secondary:  dodgerblue
   },
});


ReactDOM.render(
   <ThemeProvider theme={theme}>
      <BrowserRouter>
         <App />
      </BrowserRouter>
   </ThemeProvider>,
document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
