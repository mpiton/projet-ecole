import React, {useState} from 'react'
import { firebaseAuth } from '../../firebase'
import {Redirect} from 'react-router-dom'


const Authenticated = (props) => {
   // React Hook https://fr.reactjs.org/docs/hooks-intro.html
   const [loggedIn, setLoggedIn] = useState(null);

   firebaseAuth.onAuthStateChanged((user) => {
      if (user) {
         setLoggedIn(true)
      } else {
         setLoggedIn(false)
      }
   })

   if (props.nonAuthenticated) {
      if (loggedIn == null) {
         return "Chargement en cours...";
      } else if (!loggedIn) {
         return props.children;
      } else if (loggedIn) {
         return <Redirect to="/"/>;
      }
   } else {
      if (loggedIn == null) {
         return "Chargement en cours...";
      } else if (loggedIn) {
         return props.children;
      } else if (!loggedIn) { //Si pas loggé alors redirection page login
         return <Redirect to='/login' />;
      }
   }

};

export default Authenticated;
