import React, {useState} from 'react'
import { Redirect } from 'react-router-dom';
import firebase from '../../firebase'

const Logout = (props) => {

   const [loggedOut, setLoggedOut] = useState(null);

   firebase.auth().signOut().then(function(user) {
      if (user) {
         setLoggedOut(true)
      } else {
         setLoggedOut(false)
      }
   });

   if (props.deco) {
      if (loggedOut == null) {
         return "Chargement en cours...";
      } else if (!loggedOut) {
         return props.children;
      } else if (loggedOut) {
         return <Redirect to="/login"/>;
      }
   } else {
            if (loggedOut == null) {
         return "Chargement en cours...";
      } else if (loggedOut) {
         return props.children;
      } else if (!loggedOut) {
         return <Redirect to='/' />;
      }
   }
}

export default Logout
