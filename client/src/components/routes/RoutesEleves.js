import api from "../api/api.js";

const getAll = () => {
  return api.get("/eleves");
};

const get = id => {
  return api.get(`/eleve/${id}`);
};

const findByNom = nom => {
   return api.get(`/eleve/${nom}`);
 };

const create = data => {
  return api.post("/eleve", data);
};

const update = (id, data) => {
  return api.put(`/eleve/${id}`, data);
};

const remove = id => {
  return api.delete(`/eleve/${id}`);
};

/* eslint import/no-anonymous-default-export: [2, {"allowObject": true}] */
export default {
  getAll,
  get,
  create,
  update,
  remove,
//   paginate,
  findByNom
};
