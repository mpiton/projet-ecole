import api from "../api/api.js";

const getAll = () => {
  return api.get("/reunions");
};

const get = id => {
   return api.get(`/reunion/${id}`);
 };

const create = data => {
  return api.post("/reunion", data);
};

const remove = id => {
  return api.delete(`/reunion/${id}`);
};

const update = (id, data) => {
   return api.put(`/reunion/${id}`, data);
 };

 /* eslint import/no-anonymous-default-export: [2, {"allowObject": true}] */
export default {
  getAll,
  get,
  create,
  remove,
  update
};
