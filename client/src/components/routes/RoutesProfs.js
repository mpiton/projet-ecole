import api from "../api/api.js";

const getAll = () => {
   return api.get("/professeurs/");
};

const get = id => {
   return api.get(`/professeur/${id}`);
};

const findByNom = nom => {
   return api.get(`/professeur/nom/${nom}`);
};

const create = data => {
   return api.post("/professeur", data);
};

const update = (id, data) => {
   return api.put(`/professeur/${id}`, data);
};

const remove = id => {
   return api.delete(`/professeur/${id}`);
};

/* eslint import/no-anonymous-default-export: [2, {"allowObject": true}] */
export default {
   getAll,
   get,
   create,
   update,
   remove,
   findByNom
};
