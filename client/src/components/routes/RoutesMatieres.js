import api from "../api/api.js";

const getAll = () => {
  return api.get("/matieres/");
};

const create = data => {
  return api.post("/matiere/", data);
};

const remove = id => {
  return api.delete(`/matiere/${id}`);
};

/* eslint import/no-anonymous-default-export: [2, {"allowObject": true}] */
export default {
  getAll,
  create,
  remove,
};
