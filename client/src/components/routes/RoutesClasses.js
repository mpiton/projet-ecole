import api from "../api/api.js";

const getAll = () => {
  return api.get("/classes");
};

const create = data => {
  return api.post("/classe", data);
};

const remove = id => {
  return api.delete(`/classe/${id}`);
};

/* eslint import/no-anonymous-default-export: [2, {"allowObject": true}] */
export default {
  getAll,
  create,
  remove,
};
