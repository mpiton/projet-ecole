const express = require('express');
const morgan = require('morgan');
const colors = require('colors');
const dotenv = require('dotenv');
const connectDb = require('./config/db');
const cors = require('cors');
const helmet = require('helmet');
const rateLimit = require("express-rate-limit");
const path = require('path')
const cookieSession = require('cookie-session')

//Documentation
const swaggerUi = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc')


//Ajout valeurs d'environnement
dotenv.config({path: "./config/config.env"});

//Connexion à la DB
connectDb();

//Initialisation d'express
const app = express();

//Librairie de mise en forme des réponses HTTP sur terminal
//Si je ne suis pas en mode test, j'affiche morgan
if (process.env.NODE_ENV !== 'test') {
   app.use(morgan('dev'));
}

//Gestion Entête HTTP
const corsOptions = {
   origin: process.env.DOMAIN,
   optionsSuccessStatus: 200
}
app.use(cors(corsOptions))

//Sécurité https://helmetjs.github.io/
app.use(helmet());

//Cookies
//option de sécurité des cookies
var expiryDate = new Date( Date.now() + 60 * 60 * 1000 ); // 1 hour
app.use(cookieSession({
   name: 'session',
   saveUninitialized: true,
   resave: true,
   keys : process.env.SECRET_KEY,
   cookie: {
      secure: true,
      httpOnly: true,
      domain: process.env.DOMAIN,
      expires: expiryDate
   }
})
);
//Limitation utilisation de l'API
const limiter = rateLimit({
   windowMs: 15 * 60 * 1000, // 15 minutes
   max: 100 // limitation à 100 requêtes toutes les 15 (windowMs) minutes par ip
});
//  Application de la limitation pour toutes les request
app.use(limiter);

//body-parser
app.use(express.json());
app.use(express.json({
   extended:true
}))

// Info docu: https://swagger.io/specification/#infoObject
const options = {
   definition: {
   info: {
      version: "1.0.0",
      title: "API de l'école",
      description: "Documentation de l'API de l'école",
      contact: {
         name: "Mathieu PITON"
      },
      servers: [process.env.DOMAIN] //:5000
   }
   },
   //lien vers mes routes
   apis: ["./routes/ecole.js"]
};
const swaggerSpec = swaggerJSDoc(options);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec));

//Lien vers le router
app.use("/api", require('./routes/ecole'));

//Gestion de mise en production
if (process.env.NODE_ENV === 'production') {
   app.use(express.static('client/build'));
   app.get('*', (req, res) => {
      res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
   })
}

//Port 3333 par default puis on écoute si le serveur est lancé
const port = process.env.PORT;
app.listen(port, console.log(`Serveur lancé sur le port ${port}`.yellow.underline));

module.exports = app