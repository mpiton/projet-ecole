const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Prof = require('./Prof')

// Schema DB d'une classe
const ClasseSchema = mongoose.Schema({
   classeEleve: {
      type: String,
      required: true,
      validate: {
         validator: function(value) {
         const nomPattern = /^[a-zA-Zéè0-9_-\s]{3,6}$/;
         const nomRegExp = new RegExp(nomPattern);
         return value.match(nomRegExp);
         },
         message: props => `${props.value} est une classe invalide`
      },
   },
   profPrincipal: {
      type: Schema.Types.ObjectId,
      ref: 'Prof',
      required: true
   },
});

module.exports = Classe = mongoose.model("Classe", ClasseSchema);