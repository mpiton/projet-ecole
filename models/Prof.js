const mongoose = require('mongoose');
const Eleve = require("./Eleve")
const Classe = require('./Classe')
const Schema = mongoose.Schema;

// Schema DB d'un professeur
const ProfSchema = mongoose.Schema({
   profNom: {
      type: String,
      required: true,
      validate: {
         validator: function(value) {
         const nomPattern = /^[a-zA-Z0-9_ 'àçéèêë()+-]*$/;
         const nomRegExp = new RegExp(nomPattern);
         return value.match(nomRegExp);
         },
         message: props => `${props.value} est un nom invalide`
      },
   },
   profPrenom: {
      type: String,
      required: true,
      validate: {
         validator: function(value) {
            const nomPattern = /^[a-zA-Z0-9_ 'àçéèêë()+-]*$/;
            const nomRegExp = new RegExp(nomPattern);
            return value.match(nomRegExp);
         },
         message: props => `${props.value} n'est pas un prénom valide`
      },
   },
   profTel: {
      type: String,
      validate: {
         validator: function(value) {
           const telPattern = /^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/;
           const telRegExp = new RegExp(telPattern);
           return value.match(telRegExp);
         },
         message: props => `${props.value} n'est pas un numéro valide`
       },
   },
   profPhoto: {
      type: String,
      validate: {
         validator: function(value) {
           const urlPattern = /^(http[s]?:\/\/(www\.)?|ftp:\/\/(www\.)?|www\.){1}([0-9A-Za-z-\.@:%_\+~#=]+)+((\.[a-zA-Z]{2,3})+)(\/(.)*)?(\?(.)*)?/;
           const urlRegExp = new RegExp(urlPattern);
           return value.match(urlRegExp);
         },
         message: props => `${props.value} n'est pas une URL valide`
       },
   },
   dateArrive: {
       type: Date,
       default: Date.now
   },
   adresse: {
      codePostal: {
         type: String,
         validate: {
            validator: function(value) {
              const codePattern = /^((0[1-9])|([1-8][0-9])|(9[0-8])|(2A)|(2B))[0-9]{3}$/;
              const codeRegExp = new RegExp(codePattern);
              return value.match(codeRegExp);
            },
            message: props => `${props.value} n'est pas un code postal valide`
          },
      },
      rue: {
         type: String,
         validate: {
            validator: function(value) {
              const villePattern = /^[a-zA-Z0-9_ 'àçéèêë()+-]*$/;
              const villeRegExp = new RegExp(villePattern);
              return value.match(villeRegExp);
            },
            message: props => `${props.value} n'est pas une rue valide`
          },
      },
      ville: {
         type: String,
         validate: {
            validator: function(value) {
              const villePattern = /^[a-zA-Z0-9_ 'àçéèêë()+-]*$/;
              const villeRegExp = new RegExp(villePattern);
              return value.match(villeRegExp);
            },
            message: props => `${props.value} n'est pas une ville valide`
          },
      }
   },
   profMail: {
      type: String,
      validate: {
         validator: function(value) {
         const mailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
         const mailRegExp = new RegExp(mailPattern);
         return value.match(mailRegExp);
         },
         message: props => `${props.value} n'est pas un email valide`
      },
   },
   //Référence au schema Classe
   profClasses: {
      type: [Schema.Types.ObjectId],
      ref: 'Classe',
      required: true
   },
   //Référence au schema Matiere
   profMatiere: {
      type: [Schema.Types.ObjectId],
      ref: 'Matiere',
      required: true
   },
});

module.exports = Prof = mongoose.model("prof", ProfSchema);