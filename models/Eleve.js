const mongoose = require('mongoose');
const Classe = require('./Classe')
const Schema = mongoose.Schema;

// Schema DB d'un élève
const EleveSchema = mongoose.Schema({
   eleNom: {
      type: String,
      required: true,
      validate: {
         validator: function(value) {
           const nomPattern = /^[a-zA-Z0-9_ 'àçéèêë()+-]*$/;
           const nomRegExp = new RegExp(nomPattern);
           return value.match(nomRegExp);
         },
         message: props => `${props.value} est un nom invalide`
       },
   },
   elePrenom: {
      type: String,
      required: true,
      validate: {
         validator: function(value) {
           const nomPattern = /^[a-zA-Z0-9_ 'àçéèêë()+-]*$/;
           const nomRegExp = new RegExp(nomPattern);
           return value.match(nomRegExp);
         },
         message: props => `${props.value} est un prénom invalide`
       },
   },
   eleTel: {
      type: String,
      validate: {
         validator: function(value) {
           const telPattern = /^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/;
           const telRegExp = new RegExp(telPattern);
           return value.match(telRegExp);
         },
         message: props => `${props.value} n'est pas un numéro valide`
       },
   },
   eleTelSec: {
      type: String,
      validate: {
         validator: function(value) {
           const telPattern = /^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/;
           const telRegExp = new RegExp(telPattern);
           return value.match(telRegExp);
         },
         message: props => `${props.value} n'est pas un numéro valide`
       },
   },
   eleBirthday: {
      type: Date,
      default: Date.now
  },
   eleNotes: [{
      type: Number,
      max: [20, "La note ne peut être supérieure à 20"],
      min: [0, "La note ne peut être inférieure à 0"],
   }],
   eleAdresse: {
      codePostal: {
         type: String,
         validate: {
            validator: function(value) {
              const codePattern = /^((0[1-9])|([1-8][0-9])|(9[0-8])|(2A)|(2B))[0-9]{3}$/;
              const codeRegExp = new RegExp(codePattern);
              return value.match(codeRegExp);
            },
            message: props => `${props.value} n'est pas un code postal valide`
          },
      },
      rue: {
         type: String,
         validate: {
            validator: function(value) {
              const villePattern = /^[a-zA-Z0-9_ 'àçéèêë()+-]*$/;
              const villeRegExp = new RegExp(villePattern);
              return value.match(villeRegExp);
            },
            message: props => `${props.value} n'est pas une rue valide`
          },
      },
      ville: {
         type: String,
         validate: {
            validator: function(value) {
              const villePattern = /^[a-zA-Z0-9_ 'àçéèêë()+-]*$/;
              const villeRegExp = new RegExp(villePattern);
              return value.match(villeRegExp);
            },
            message: props => `${props.value} n'est pas une ville valide`
          },
      }
   },
   eleComment: {
      type: String,
      maxlength: [1500, "Les commentaires ne peuvent excéder 1500 caractères"],
      validate: {
         validator: function(value) {
           const nomPattern = /^[a-zA-Z0-9_ 'àçéèêë()+-]*$/;
           const nomRegExp = new RegExp(nomPattern);
           return value.match(nomRegExp);
         },
         message: props => `${props.value} n'est pas un commentaire valide`
       },
   },
   //Référence au schema Classe
   eleClasse: {
      type: Schema.Types.ObjectId,
      ref: 'Classe',
      required: true,
      unique: true,
      index: true,
      sparse: true
   },
});

module.exports = Eleve = mongoose.model("eleve", EleveSchema);