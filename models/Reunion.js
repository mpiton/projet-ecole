const mongoose = require('mongoose');

// Schema DB d'une Réunion
const ReunionSchema = mongoose.Schema({
   title: {
      type: String,
      required: true,
      maxlength: [125, "La longueur du titre ne peut excéder 125 caractères"],
      validate: {
         validator: function(value) {
         const nomPattern = /^[a-zA-Z0-9_ 'àçéèêë()+-]*$/;
         const nomRegExp = new RegExp(nomPattern);
         return value.match(nomRegExp);
         },
         message: props => `${props.value} est un titre invalide`
      },
   },
   date: {
      type: Date,
      required: true
   }
});

module.exports = Reunion = mongoose.model("reunion", ReunionSchema);