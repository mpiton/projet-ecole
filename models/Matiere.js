const mongoose = require('mongoose');

// Schema DB d'une matiere
const MatiereSchema = mongoose.Schema({
   nomMatiere: {
      type: String,
      required: true,
      validate: {
         validator: function(value) {
           const nomPattern = /^[a-zA-Z0-9_ éçè]*$/;
           const nomRegExp = new RegExp(nomPattern);
           return value.match(nomRegExp);
         },
         message: props => `${props.value} est une matière invalide`
       },
   },
});

module.exports = Matiere = mongoose.model("matiere", MatiereSchema);