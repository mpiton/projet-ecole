const express = require('express');
const router = express.Router();
require('../server')

//Import des models
const Eleve = require('../models/Eleve')
const Prof = require('../models/Prof');
const Reunion = require('../models/Reunion');
const Classe = require('../models/Classe');
const Matiere = require('../models/Matiere');


//
// ─── ELEVES ─────────────────────────────────────────────────────────────────────
//
/**
 * @swagger
 * /api/eleve:
 *  post:
 *    tags:
 *       - Eleves
 *    description: création d'un élève
 *    parameters:
 *       - name: eleNom
 *         description: Nom de l'élève
 *         in: path
 *         type: string
 *         required: true
 *       - name: elePrenom
 *         description: Prénom de l'élève
 *         in: path
 *         type: string
 *         required: true
 *       - name: eleTel
 *         description: Téléphone de l'élève'
 *         in: path
 *         type: string
 *       - name: eleTelSec
 *         description: Téléphone de secours
 *         in: path
 *         type: string
 *       - name: eleBirthday
 *         description: Date de naissance de l'élève
 *         in: path
 *         type: date
 *       - name: eleComment
 *         description: Commentaires importants concernant l'élève
 *         in: path
 *         type: string
 *       - name: eleClasse
 *         description: id de la classe assignée à l'élève
 *         in: path
 *         type: string
 *         required: true
 *         unique: true
 *    responses:
 *      '201':
 *        description: L'élève est créé
 *      '401':
 *        description: Erreur dans la sauvegarde
 */
router.post('/eleve', (req, res) => {
   //Création d'un nouvel élève
   var newEleve = new Eleve(req.body);
   //Save dans la DB.
   newEleve.save((err,eleve) => {
      if(err) {
         res.status(401)
         res.json(err)
      }
      else { //Si pas d'erreur on renvoi cette réponse
         res.status(201)
         res.json({
            message: "Elève ajouté avec succès !",
            sucess: true,
            eleve: eleve
            })
      }
   });
})

/**
 * @swagger
 * /api/eleves:
 *  get:
 *    tags:
 *       - Eleves
 *    description: récupération de tous les élèves
 *    responses:
 *      '200':
 *        description: Liste des élèves récupérée
 *      '404':
 *        description: "La liste n'a pas pu être trouvé"
 */
router.get("/eleves", async (req, res, next) => {
   try {
   const eleve = await Eleve.find({});
   res.json({
      sucess: true,
      total: eleve.length,
      eleves: eleve,
      message: 'Liste des élèves récupérée'
   });
   } catch (error) {
   res.status(404)
   res.json({
      sucess: false,
      message: 'La liste n\'a pas pu être trouvé'
   })
   }
})

/**
 * @swagger
 * /api/eleve/{id}:
 *  get:
 *    tags:
 *       - Eleves
 *    description: récupération d'un élève par son ID
 *    parameters:
 *       - name: id
 *         description: ID de l'élève
 *         in: path
 *         type: string
 *         required: true
 *    responses:
 *       '200':
 *          description: éléve trouvé
 *       '404':
 *          description: L'ID de l'élève n'a pas été trouvé
 */
router.get("/eleve/:id", async (req, res, next) => {
   try {
      //Vérifie si l'élève existe ou pas
      let eleve = await Eleve.findById(req.params.id);
      if (!eleve) {
         res.status(404)
         return res.json({
            sucess: false,
            message: "L'ID de l'élève n'a pas été trouvé."
         });
      } else {
         res.status(200).json({
            sucess: true,
            message: "Élève trouvé !",
            eleve: eleve
         })
      }
   } catch (error) {
      next(error);
   }
});

/**
 * @swagger
 * /api/eleve/{id}:
 *  put:
 *    tags:
 *       - Eleves
 *    description: Mise à jour d'un élève par son ID
 *    parameters:
 *       - name: id
 *         description: ID de l'élève
 *         in: path
 *         type: string
 *         required: true
 *       - name: Requête
 *         description: Requête dans le body
 *         in: body
 *         schema:
 *          type: object
 *          properties:
 *             eleNom:
 *                type: string
 *                required: true
 *                description: Nom de l'élève
 *             elePrenom:
 *                type: string
 *                required: true
 *                description: Nom de l'élève
 *             eleTel:
 *                type: string
 *                description: Téléphone des parents de l'élève
 *             eleTelSec:
 *                type: string
 *                description: Téléphone secondaire des parents de l'élève
 *             eleBirthday:
 *                type: date
 *                description: date de naissance de l'élève
 *             eleComment:
 *                type: string
 *                description: Espace pour récupérer des informations importante sur l'élève
 *             eleClasse:
 *                type: object
 *                properties:
 *                   _id: string
 *                required:
 *                   - _id
 *                description: ID qui fait référence au Schema Classe
 *    responses:
 *       '200':
 *          description: éléve mis à jour
 *       '404':
 *          description: L'ID de l'élève n'a pas été trouvé
 *       '401':
 *          description: Autres erreurs
 */
router.put("/eleve/:id", async (req, res, next) => {
   try {
           //Vérifie si l'élève existe ou pas
         let eleve = await Eleve.findById(req.params.id);
         if (!eleve) {
            res.status(404)
            return res.json({
               sucess: false,
               message: "L'ID de l'élève n'a pas été trouvé."
            })
         }else {
            let majEleve = await Eleve.findByIdAndUpdate(req.params.id, req.body, {
               new: true,
               runValidators:true
            });
            res.json({
               sucess: true,
               message: "Les données de l'élève ont été mise à jour.",
               eleve: majEleve
            })
         }
   }
   catch (error) {
      res.status(401)
      res.json({
      sucess: false,
      message: error.message
      });
   }
})

/**
 * @swagger
 * /api/eleve/{id}:
 *  delete:
 *    tags:
 *       - Eleves
 *    description: suppression d'un élève par son ID
 *    parameters:
 *       - name: id
 *         description: ID de l'élève
 *         in: path
 *         type: string
 *         required: true
 *    responses:
 *       '200':
 *          description: éléve supprimé
 *       '404':
 *          description: L'ID de l'élève n'a pas été trouvé
 *       '401':
 *          description: Autres erreurs
 */
router.delete("/eleve/:id", async (req, res, next) => {
   try {
           //Vérifie si l'élève existe ou pas
         let eleve = await Eleve.findById(req.params.id);
         if (!eleve) {
            res.status(404)
            return res.json({
               sucess: false,
               message: "L'ID de l'élève n'a pas été trouvé."
            })
         } else {
            await eleve.remove();
            res.json({
               sucess: true,
               message: `L'élève avec l'ID ${req.params.id} été supprimé`,
               eleve: {}
            })
         }
   }
   catch (error) {
      res.status(401)
      next(error);
   }
});

/**
 * @swagger
 * /api/eleve/nom/{nom}:
 *  get:
 *    tags:
 *       - Eleves
 *    description: récupération d'un élève par son nom
 *    parameters:
 *       - name: nom
 *         description: nom de l'élève
 *         in: path
 *         type: string
 *         required: true
 *    responses:
 *       '200':
 *          description: Elève trouvé !
 *       '404':
 *          description: L'élève n'a pas été trouvé
 *       '401':
 *          description: Autres erreurs
 */
router.get("/eleve/nom/:elevenom", async (req, res, next) => {
   try {
      //Verrouillage du parametre dans une constante
      const nom = req.params.elevenom
      if(nom != null){
         let eleve = await Eleve.find({eleNom: nom});
            //Vérifie si l'élève existe ou pas
            if (eleve.length <= 0) {
               res.status(404)
               return res.json({
               sucess: false,
               message: "L'élève n'a pas été trouvé."
            });
         } else {
               res.json({
               sucess: true,
               message: "Elève trouvé !",
               eleve: eleve
            })
         }
      }else{
         res.end();
      }
   } catch (error) {
      res.status(401)
      next(error);
   }
})

//
// ─── PROFESSEURS ────────────────────────────────────────────────────────────────
//
/**
 * @swagger
 * /api/professeur:
 *  post:
 *    tags:
 *       - Professeurs
 *    description: création d'un professeur
 *    parameters:
 *       - name: profNom
 *         description: Nom du professeur
 *         in: path
 *         type: string
 *         required: true
 *       - name: profPrenom
 *         description: Prénom du professeur
 *         in: path
 *         type: string
 *         required: true
 *       - name: profTel
 *         description: Téléphone du professeur
 *         in: path
 *         type: string
 *       - name: profPhoto
 *         description: URL de la photo du professeur
 *         in: path
 *         type: string
 *       - name: dateArrive
 *         description: Date d'arrivée du professeur
 *         in: path
 *         type: date
 *       - name: profMail
 *         description: Email du professeur
 *         in: path
 *         type: string
 *       - name: profClasses
 *         description: id de la Classe assignée au professeur
 *         in: path
 *         type: string
 *         required: true
 *       - name: profMatiere
 *         description: id de la matière enseignée par professeur
 *         in: path
 *         type: string
 *         required: true
 *    responses:
 *      '201':
 *        description: Le professeur est créé
 *      '401':
 *        description: Erreur lors de la sauvegarde
 */
router.post('/professeur', (req, res) => {
   //Création d'un nouveau prof
   var newProf = new Prof(req.body);
   //Save dans la DB.
   newProf.save((err,prof) => {
      if(err) {
         res.status(401)
         return res.json(err)
      }
       else { //Si pas d'erreur on renvoi cette réponse
         res.status(201)
         res.json({
            message: "Professeur ajouté avec succès !",
            sucess: true,
            prof: prof
            })
      }
   });
})

/**
 * @swagger
 * /api/professeurs:
 *  get:
 *    tags:
 *       - Professeurs
 *    description: récupération de tous les professeurs
 *    responses:
 *      '200':
 *        description: Liste des professeurs récupérée
 *      '404':
 *        description: "La liste n'a pas pu être trouvé"
 */
router.get("/professeurs", async (req, res, next) => {
   try {
      const prof = await Prof.find({});
      res.json({
      sucess: true,
      total: prof.length,
      profs: prof,
      message: 'Liste des professeurs récupérée'
   })
   } catch (error) {
      res.status(404)
      res.json({
         sucess: false,
         message: 'La liste n\'a pas pu être trouvé'
      })
   }
});



/**
 * @swagger
 * /api/professeur/{id}:
 *  get:
 *    tags:
 *       - Professeurs
 *    description: récupération d'un professeur par son ID
 *    parameters:
 *       - name: id
 *         description: ID du professeur
 *         in: path
 *         type: string
 *         required: true
 *    responses:
 *       '200':
 *          description: professeur trouvé
 *       '404':
 *          description: L'ID du professeur n'a pas été trouvé
 */
router.get("/professeur/:id", async (req, res, next) => {
   try {
      //Vérifie si le professeur existe ou pas
      let prof = await Prof.findById(req.params.id);
      if (!prof) {
         res.status(404)
         return res.json({
            sucess: false,
            message: "L'ID du professeur n'a pas été trouvé."
         });
      } else {
         res.json({
            sucess: true,
            message: "Professeur trouvé !",
            prof: prof
         })
      }
   } catch (error) {
      next(error);
   }
})



/**
 * @swagger
 * /api/professeur/{id}:
 *  put:
 *    tags:
 *       - Professeurs
 *    description: Mise à jour d'un professeurs par son ID
 *    parameters:
 *       - name: id
 *         description: ID du professeur
 *         in: path
 *         type: string
 *         required: true
 *       - name: Requête
 *         description: Requête dans le body
 *         in: body
 *         schema:
 *          type: object
 *          properties:
 *             profNom:
 *                type: string
 *                required: true
 *                description: Nom du professseur
 *             profPrenom:
 *                type: string
 *                required: true
 *                description: Nom du professeur
 *             profTel:
 *                type: string
 *                description: Téléphone du professeur
 *             profPhoto:
 *                type: string
 *                description: URL de la photo de profil du professeur
 *             dateArrive:
 *                type: date
 *                description: date d'arrivée dans l'école
 *             profMail:
 *                type: string
 *                description: Email du professeur
 *             profMatiere:
 *                type: object
 *                properties:
 *                   _id: string
 *                required: true
 *                description: ID qui fait référence au Schema Matiere
 *             profClasse:
 *                type: object
 *                properties:
 *                   _id: string
 *                required: true
 *                description: ID qui fait référence au Schema Classe
 *    responses:
 *       '200':
 *          description: éléve mis à jour
 *       '404':
 *          description: L'ID de l'élève n'a pas été trouvé
 *       '401':
 *          description: Autres erreurs
 */
router.put("/professeur/:id", async (req, res, next) => {
   try {
         //Vérifie si le prof existe ou pas
         let prof = await Prof.findById(req.params.id);
         if (!prof) {
            res.status(404)
            return res.json({
               sucess: false,
               message: "L'ID du professeur n'a pas été trouvé."
            });
         } else {
            let majProf = await Prof.findByIdAndUpdate(req.params.id, req.body, {
               new: true,
               runValidators:true
            });
            res.json({
               sucess: true,
               message: "Les données du professeur ont été mise à jour.",
               prof: majProf
            })
         }
   }
   catch (error) {
      res.status(401)
      res.json({
      sucess: false,
      message: error.message
});
   }
})



/**
 * @swagger
 * /api/professeur/{id}:
 *  delete:
 *    tags:
 *       - Professeurs
 *    description: suppression d'un professeur par son ID
 *    parameters:
 *       - name: id
 *         description: ID du professeur
 *         in: path
 *         type: string
 *         required: true
 *    responses:
 *       '200':
 *          description: professeur supprimé
 *       '404':
 *          description: L'ID du professeur n'a pas été trouvé
 *       '401':
 *          description: Autres erreurs
 */
router.delete("/professeur/:id", async (req, res, next) => {
   try {
         //Vérifie si le professeur existe ou pas
         let prof = await Prof.findById(req.params.id);
         if (!prof) {
            res.status(404);
            return res.json({
               sucess: false,
               message: "L'ID du professeur n'a pas été trouvé."
            })
         } else {
            await prof.remove();
            res.json({
               sucess: true,
               message: `Le professeur avec l'ID ${req.params.id} été supprimé`,
               prof: {}
            })
         }
   } catch (error) {
      res.status(401)
      next(error);
   }
})

/**
 * @swagger
 * /api/professeur/nom/{nom}:
 *  get:
 *    tags:
 *       - Professeurs
 *    description: récupération d'un professeur par son nom
 *    parameters:
 *       - name: nom
 *         description: nom du professeur
 *         in: path
 *         type: string
 *         required: true
 *    responses:
 *       '200':
 *          description: Professeur trouvé !
 *       '404':
 *          description: Le professeur n'a pas été trouvé
 *       '401':
 *          description: Autres erreurs
 */
router.get("/professeur/nom/:profnom", async (req, res, next) => {
   try {
      //Verrouillage du parametre dans une constante
      const nom = req.params.profnom
      if(nom != null){
         let prof = await Prof.find({profNom: nom});
            //Vérifie si le professeur existe ou pas
            if (prof.length <= 0) {
               res.status(404)
               return res.json({
               sucess: false,
               message: "Le professeur n'a pas été trouvé."
            });
         } else {
               res.json({
               sucess: true,
               message: "Professeur trouvé !",
               prof: prof
            })
         }
      }else{
         res.end();
      }
   } catch (error) {
      res.status(401)
      next(error);
   }
})

//
// ─── REUNIONS ───────────────────────────────────────────────────────────────────
//
/**
 * @swagger
 * /api/reunions:
 *  get:
 *    tags:
 *       - Réunions
 *    description: récupération des toutes les réunions
 *    responses:
 *      '200':
 *        description: Liste des réunions récupérée
 *      '404':
 *        description: "La liste n'a pas pu être trouvé"
 */
router.get("/reunions", async (req, res, next) => {
   try {
      const reunion = await Reunion.find({})
      res.json({
         sucess: true,
         total: reunion.length,
         reunions: reunion,
         message: 'Liste des réunions récupérée'
      })
   } catch (error) {
      res.status(404)
      res.json({
         sucess: false,
         message: 'La liste n\'a pas pu être trouvé'
      })
   }
})

/**
 * @swagger
 * /api/reunion/{id}:
 *  put:
 *    tags:
 *       - Réunions
 *    description: Mise à jour d'une réunion par son ID
 *    parameters:
 *       - name: id
 *         description: ID de la réunion
 *         in: path
 *         type: string
 *         required: true
 *       - name: Requête
 *         description: Requête dans le body
 *         in: body
 *         schema:
 *          type: object
 *          properties:
 *             title:
 *                type: string
 *                required: true
 *                description: Titre de la réunion
 *             date:
 *                type: date
 *                required: true
 *                description: Date de la réunion
 *    responses:
 *       '200':
 *          description: éléve mis à jour
 *       '404':
 *          description: L'ID de l'élève n'a pas été trouvé
 *       '401':
 *          description: Autres erreurs
 */
router.put("/reunion/:id", async (req, res, next) => {
   try {
         //Vérifie si le prof existe ou pas
         let reunion = await Reunion.findById(req.params.id);
         //La réunion n'existe pas
         if (!reunion) {
            res.status(404)
            return res.json({
               sucess: false,
               message: "L'ID de la réunion n'a pas été trouvé."
            })
              //La réunion existe
         } else {
            let majReunion = await Reunion.findByIdAndUpdate(req.params.id, req.body, {
               new: true,
               runValidators:true
            });
            res.json({
               sucess: true,
               message: "Les données de la réunion ont été mise à jour.",
               reunion: majReunion
            })
         }
   } catch (error) {
      //Erreur de validation ou tout autres erreurs
      res.status(401)
      res.json({
         sucess: false,
         message: error.message
      });
   }
})

/**
 * @swagger
 * /api/reunion:
 *  post:
 *    tags:
 *       - Réunions
 *    description: création d'une réunion
 *    parameters:
 *       title:
 *          type: string
 *          required: true
 *          description: Titre de la réunion
 *       date:
 *          type: date
 *          required: true
 *          description: Date de la réunion
 *    responses:
 *      '201':
 *        description: La réunion est créé
 *      '401':
 *        description: Erreur de saisi
 */
router.post('/reunion', (req, res, next) => {
   //Création d'une nouvelle réunion
   var newReunion = new Reunion(req.body);
   //Save dans la DB.
   newReunion.save((err,reunion) => {
      if(err) {
         res.status(401)
         return res.json(err)
      }
      else { //Si pas d'erreur on renvoi cette réponse
         res.status(201)
         res.json({
            message: "Réunion ajoutée avec succès !",
            sucess: true,
            reunion: reunion
         })
      }
   });
});

/**
 * @swagger
 * /api/reunion/{id}:
 *  delete:
 *    tags:
 *       - Réunions
 *    description: suppression d'une réunion par son ID
 *    parameters:
 *       - name: id
 *         description: ID de la réunion
 *         in: path
 *         type: string
 *         required: true
 *    responses:
 *       '200':
 *          description: réunion supprimée
 *       '404':
 *          description: L'ID de la réunion n'a pas été trouvé
 *       '401':
 *          description: Autres erreurs
 */
router.delete("/reunion/:id", async (req, res, next) => {
   try {
         //Vérifie si la réunion existe ou pas
         let reunion = await Reunion.findById(req.params.id);
         if (!reunion) {
            res.status(404)
            return res.json({
               sucess: false,
               message: "La réunion n'a pas été trouvé."
            })
         } else {
            await reunion.remove();
            res.json({
               sucess: true,
               message: `La réunion avec l'ID ${req.params.id} été supprimée`,
               reunion: {}
            })
         }
   }
   catch (error) {
      res.status(401)
      next(error);
   }
})

/**
 * @swagger
 * /api/reunion/{id}:
 *  get:
 *    tags:
 *       - Réunions
 *    description: récupération d'une réunion par son ID
 *    parameters:
 *       - name: id
 *         description: ID de la réunion
 *         in: path
 *         type: string
 *         required: true
 *    responses:
 *       '200':
 *          description: réunion trouvé
 *       '404':
 *          description: L'ID de la réunion n'a pas été trouvé
 *       '401':
 *          description: Autres erreurs
 */
router.get("/reunion/:id", async (req, res, next) => {
   try {
      //Vérifie si la reunion existe ou pas
      let reunion = await Reunion.findById(req.params.id);
      if (!reunion) {
         res.status(404)
         return res.json({
            sucess: false,
            message: "L'ID du réunion n'a pas été trouvé."
         });
      } else {
         res.json({
            sucess: true,
            message: "Réunion trouvé !",
            reunion: reunion
         })
      }
   } catch (error) {
      res.status(401)
      next(error);
   }
})

//
// ─── CLASSES ────────────────────────────────────────────────────────────────────
//

   /**
 * @swagger
 * /api/classes:
 *  get:
 *    tags:
 *       - Classes
 *    description: récupération des toutes les classes
 *    responses:
 *      '200':
 *        description: Liste des classes récupérée
 *      '404':
 *        description: "La liste n'a pas pu être trouvé"
 */
router.get("/classes", async (req, res, next) => {
   try {
      const classes = await Classe.find({});
      res.json({
         sucess: true,
         total: classes.length,
         classes: classes
      });
   } catch (error) {
      res.status(404)
      res.json({
         sucess: false,
         message: 'La liste n\'a pas pu être trouvé'
      })
   }
})

/**
 * @swagger
 * /api/classe:
 *  post:
 *    tags:
 *       - Classes
 *    description: création d'une classe
 *    parameters:
 *       classeEleve:
 *          type: string
 *          required: true
 *          description: Nom de la classe
 *       profPrincipal:
 *          type: string
 *          required: true
 *          description: Id du professeur qui est en charge de cette classe
 *    responses:
 *      '201':
 *        description: La classe est crée
 *      '401':
 *        description: Autres erreurs
 */
router.post('/classe', async (req, res, next) => {
   try{
      const classe = await Classe.findOne({classeEleve: req.body.classeEleve});
         if (classe) {
            res.status(401)
            return res.json({
               sucess: false,
               message: 'La classe existe déjà'
            })
         } else { //Sinon on l'ajoute à la DB
            let newClasse = await Classe.create(req.body);
            res.status(201)
            res.json({
               sucess: true,
               message: 'Classe ajoutée avec succès !',
               classe: newClasse
            })
         }
      }
   catch(err){
         res.status(401)
         next(err);
      }
});

/**
 * @swagger
 * /api/classe/{id}:
 *  delete:
 *    tags:
 *       - Classes
 *    description: suppression d'une classe par son ID
 *    parameters:
 *       - name: id
 *         description: ID de la classe
 *         in: path
 *         type: string
 *         required: true
 *    responses:
 *       '200':
 *          description: classe supprimée
 *       '404':
 *          description: L'ID de la classe n'a pas été trouvé
 */
router.delete("/classe/:id", async (req, res, next) => {
   try {
         //Vérifie si la réunion existe ou pas
         let classe = await Classe.findById(req.params.id);
         if (!classe) {
            res.status(404);
            res.json({
            sucess: false,
            message: "La classe n'a pas été trouvé."
            })
         } else {
            await classe.remove();
            res.json({
               sucess: true,
               message: `La classe avec l'ID ${req.params.id} été supprimée`,
               classe: {}
            })
         }
   } catch (error) {
      res.status(401)
      next(error);
   }
})

//
// ─── MATIERES ───────────────────────────────────────────────────────────────────
//
   /**
 * @swagger
 * /api/matieres:
 *  get:
 *    tags:
 *       - Matières
 *    description: récupération des toutes les matières
 *    responses:
 *      '200':
 *        description: Liste des matières récupérée
 *      '401':
 *        description: Autres erreurs
 */
router.get("/matieres", async (req, res, next) => {
   try {
      const matieres = await Matiere.find({});
      res.json({
         sucess: true,
         total: matieres.length,
         matieres: matieres
      });
   } catch (error) {
         res.status(401)
         next(error);
   }
})

/**
 * @swagger
 * /api/matiere:
 *  post:
 *    tags:
 *       - Matières
 *    description: création d'une matière
 *    parameters:
 *       - name: nomMatiere
 *         description: Nom de la matière
 *         in: path
 *         type: string
 *         required: true
 *         unique: true
 *    responses:
 *      '201':
 *        description: La matière est crée
 *      '401':
 *        description: Erreur de saisi
 */
router.post('/matiere', async (req, res, next) => {
   try{
      const matiere = await Matiere.findOne({nomMatiere: req.body.nomMatiere});
      if (matiere) { //Si la matiere existe
         res.status(401)
         return res.json({
            sucess: false,
            message: 'La matière existe déjà'
         })
      } else { //Sinon on l'ajoute à la DB
         let newMatiere = await Matiere.create(req.body);
         res.status(201)
         res.json({
            sucess: true,
            message: 'Matière ajoutée avec succès !',
            matiere: newMatiere
         })
      }
   }catch(err){
      res.status(401)
      next(err);
   }
});

/**
 * @swagger
 * /api/matiere/{id}:
 *  delete:
 *    tags:
 *       - Matières
 *    description: suppression d'une matière par son ID
 *    parameters:
 *       - name: id
 *         description: ID de la matière
 *         in: path
 *         type: string
 *         required: true
 *    responses:
 *       '200':
 *          description: matière supprimée
 *       '404':
 *          description: Autres erreurs
 */
router.delete("/matiere/:id", async (req, res, next) => {
   try {
         //Vérifie si la réunion existe ou pas
         let matiere = await Matiere.findById(req.params.id);
         if (!matiere) {
            res.status(404)
            return res.json({
               sucess: false,
               message: "La matiere n'a pas été trouvé."
            })
         } else {
            await matiere.remove();
            res.json({
               sucess: true,
               message: `La matiere avec l'ID ${req.params.id} été supprimée`,
               matiere: {}
            })
         }
   } catch (error) {
      res.status(401)
      next(error);
   }
})


module.exports = router;
