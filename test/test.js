const mongoose = require("mongoose");
const Prof = require('../models/Prof')
const Eleve = require('../models/Eleve')
const Reunion = require('../models/Reunion')

//pour disable morgan
process.env.NODE_ENV = 'test';

//import de chai
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);

//
// ─── MON BLOCK PROFESSEURS ──────────────────────────────────────────────────────
//
describe('Test API Professeurs', () => {
   beforeEach((done) => { //Avant chaques test on vide la DB
      Prof.deleteMany({}, (err) => {
         if (err) throw err
         done();
      });
   });

   /*
     * Test /GET ALL
    */
   describe('/GET Professeurs', () => {
      it('doit récupérer tous les professeurs', (done) => {
         chai.request(server)
            .get('/api/professeurs')
            .end((err, res) => {
                  if (err) throw err
                  res.should.have.status(200);
                  res.body.should.have.property('profs').which.is.an('array')
                  res.body.should.have.property('total').to.equal(0)
            done();
            });
      });
   });


   /*
   * Test /POST Professeur
   */
   describe('/POST Professeur', () => {
      it('doit ajouter un professeur', (done) => {
         let professeur = {
            profNom: "Doe",
            profPrenom: "John",
            profClasses: ["5f82c16d0a5a0a243726df24"],
            profMatiere: ["5f8dec292b74269bf5de8117"]
         }
         chai.request(server)
         .post('/api/professeur')
         .send(professeur)
         .end((err, res) => {
            if (err) throw err
            res.should.have.status(201);
            res.body.should.be.a('object');
            res.body.should.have.property('prof')
            res.body.should.have.property('sucess').eql(true);
            res.body.should.have.property('message').eql("Professeur ajouté avec succès !")
            done();
         });
      });

   it('ne doit pas ajouter un professeur car champs manquant', (done) => {
      let professeur = {
         profPrenom: "John",
         profClasses: ["5f82c16d0a5a0a243726df24"],
         profMatiere: ["5f8dec292b74269bf5de8117"]
      }
      chai.request(server)
      .post('/api/professeur')
      .send(professeur)
      .end((err, res) => {
         if (err) throw err
         res.should.have.status(401);
         res.body.should.be.a('object');
         res.body.should.have.property('errors')
         res.body.errors.should.have.property('profNom');
         res.body.errors.profNom.should.have.property('kind').eql("required")
         done();
      });
   });

   it('ne doit pas ajouter un professeur car erreur validation', (done) => {
      let professeur = {
         profNom: 'Doe<br>',
         profPrenom: "John",
         profClasses: ["5f82c16d0a5a0a243726df24"],
         profMatiere: ["5f8dec292b74269bf5de8117"]
      }
      chai.request(server)
      .post('/api/professeur')
      .send(professeur)
      .end((err, res) => {
         if (err) throw err
         res.should.have.status(401);
         res.body.should.be.a('object');
         res.body.should.have.property('errors')
         res.body.errors.should.have.property('profNom');
         res.body.errors.profNom.should.have.property('message').eql("Doe<br> est un nom invalide")
         done();
      });
   });
   });

   /*
   * Test /GET/:id Professeur
   */
   describe('/GET/:id Professeur', () => {
      it('doit récupérer un professeur par son id', (done) => {
      let professeur = new Prof({
         profNom: "Doe",
         profPrenom: "John",
         profClasses: ["5f82c16d0a5a0a243726df24"],
         profMatiere: ["5f8dec292b74269bf5de8117"]
      });
      professeur.save((err, professeur) => {
         if (err) throw err
         chai.request(server)
         .get('/api/professeur/' + professeur.id)
         .send(professeur)
         .end((err, res) => {
            if (err) throw err
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('sucess').eql(true);
            res.body.should.have.property('message').eql("Professeur trouvé !");
            res.body.should.have.property('prof');
            res.body.prof.should.have.property('_id').eql(professeur.id);
            done();
         });
      });
      });
   });

   /*
   * Test /GET/:nom Professeur
   */
   describe('/GET/:nom Professeur', () => {
      it('doit récupérer un professeur par son nom', (done) => {
      let professeur = new Prof({
         profNom: "Doe",
         profPrenom: "John",
         profClasses: ["5f82c16d0a5a0a243726df24"],
         profMatiere: ["5f8dec292b74269bf5de8117"]
      });
      professeur.save((err, professeur) => {
         if (err) throw err
         chai.request(server)
         .get('/api/professeur/nom/' + professeur.profNom)
         .send(professeur)
         .end((err, res) => {
            if (err) throw err
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('sucess').eql(true);
            res.body.should.have.property('message').eql("Professeur trouvé !");
            res.body.should.have.property('prof')
            res.body.prof.should.be.a('array')
            res.body.prof[0].should.have.property('profNom').eql(professeur.profNom)
            done();
         });
      });
      });
   });

   /*
   * Test /PUT/:id Professeur
   */
   describe('/PUT/:id Professeur', () => {
      it('doit UPDATE un professeur par son id', (done) => {
         let professeur = new Prof({
            profNom: "Doe",
            profPrenom: "John",
            profClasses: ["5f82c16d0a5a0a243726df24"],
            profMatiere: ["5f8dec292b74269bf5de8117"]
         });
      professeur.save((err, professeur) => {
         if (err) throw err
         chai.request(server)
         .put('/api/professeur/' + professeur.id)
         .send({
            profNom: "Dodo",
            profPrenom: "Jane",
            profClasses: ["5f82c16d0a5a0a243726df24"],
            profMatiere: ["5f8dec292b74269bf5de8117"]
         })
         .end((err, res) => {
            if (err) throw err
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Les données du professeur ont été mise à jour.');
            res.body.prof.should.have.property('profNom').eql("Dodo");
            res.body.prof.should.have.property('profPrenom').eql("Jane");
            done();
         });
      });
      });

      it('ne doit pas UPDATE un professeur car erreur validation', (done) => {
         let professeur = new Prof({
            profNom: "Doe",
            profPrenom: "John",
            profClasses: ["5f82c16d0a5a0a243726df24"],
            profMatiere: ["5f8dec292b74269bf5de8117"]
         });
      professeur.save((err, professeur) => {
         if (err) throw err
         chai.request(server)
         .put('/api/professeur/' + professeur.id)
         .send({
            profNom: "Dodo",
            profPrenom: "Jane<script>",
            profClasses: ["5f82c16d0a5a0a243726df24"],
            profMatiere: ["5f8dec292b74269bf5de8117"]
         })
         .end((err, res) => {
            if (err) throw err
            res.should.have.status(401);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Validation failed: profPrenom: Jane<script> n\'est pas un prénom valide');
            res.body.should.have.property('sucess').eql(false);
            done();
         });
      });
      });
   });

   /*
   * Test /DELETE/:id Professeur
   */
   describe('/DELETE/:id Professeur', () => {
      it('doit supprimer un professeur par son id', (done) => {
         let professeur = new Prof({
            profNom: "Doe",
            profPrenom: "John",
            profClasses: ["5f82c16d0a5a0a243726df24"],
            profMatiere: ["5f8dec292b74269bf5de8117"]
         });
         professeur.save((err, professeur) => {
            if (err) throw err
            chai.request(server)
            .delete('/api/professeur/' + professeur.id)
            .end((err, res) => {
               if (err) throw err
               res.should.have.status(200);
               res.body.should.be.a('object');
               res.body.should.have.property('message').eql(`Le professeur avec l'ID ${professeur.id} été supprimé`);
               res.body.should.have.property("prof").eql({});
               res.body.should.have.property('sucess').eql(true);
               done();
            });
         });
      });
   });
});


//
// ─── MON BLOCK ELEVES ───────────────────────────────────────────────────────────
//
describe('Test API Elèves', () => {
   beforeEach((done) => { //Avant chaques test on vide la DB
      Eleve.deleteMany({}, (err) => {
         done();
      });
   });

   /*
     * Test /GET ALL
    */
   describe('/GET Eleves', () => {
      it('doit récupérer tous les élèves', (done) => {
         chai.request(server)
            .get('/api/eleves')
            .end((err, res) => {
               if (err) throw err
               res.should.have.status(200);
               res.body.should.have.property('eleves').which.is.an('array')
               res.body.should.have.property('total').to.equal(0)
               done();
            });
      });
   });

   /*
   * Test /POST Eleve
   */
   describe('/POST Eleve', () => {
   it('doit ajouter un élève', (done) => {
      let eleve = {
         eleNom: "Doe",
         elePrenom: "John",
         eleClasse: "5f82c16d0a5a0a243726df24"
      }
      chai.request(server)
      .post('/api/eleve')
      .send(eleve)
      .end((err, res) => {
         if (err) throw err
         res.should.have.status(201);
         res.body.should.be.a('object');
         res.body.should.have.property('eleve')
         res.body.should.have.property('sucess').eql(true);
         res.body.should.have.property('message').eql("Elève ajouté avec succès !")
         done();
      });
   });

   it('ne doit pas ajouter un élève car champs manquant', (done) => {
      let eleve = {
         elePrenom: "John",
         eleClasse: "5f82c16d0a5a0a243726df24"
      }
      chai.request(server)
      .post('/api/eleve')
      .send(eleve)
      .end((err, res) => {
         if (err) throw err
         res.should.have.status(401);
         res.body.should.be.a('object');
         res.body.should.have.property('errors')
         res.body.errors.should.have.property('eleNom');
         res.body.errors.eleNom.should.have.property('kind').eql("required")
         done();
      });
   });

   it('ne doit pas ajouter un élève car erreur validation', (done) => {
      let eleve = {
         eleNom: "Doe<script>",
         elePrenom: "John",
         eleClasse: "5f82c16d0a5a0a243726df24"
      }
      chai.request(server)
      .post('/api/eleve')
      .send(eleve)
      .end((err, res) => {
         if (err) throw err
         res.should.have.status(401);
         res.body.should.be.a('object');
         res.body.should.have.property('errors')
         res.body.errors.should.have.property('eleNom');
         res.body.errors.eleNom.should.have.property('message').eql("Doe<script> est un nom invalide")
         done();
      });
   });
});

   /*
   * Test /GET/:id Elève
   */
  describe('/GET/:id Elève', () => {
   it('doit récupérer un élève par son id', (done) => {
   let eleve = new Eleve({
      eleNom: "Doe",
      elePrenom: "John",
      eleClasse: "5f82c16d0a5a0a243726df24"
   });
   eleve.save((err, eleve) => {
      if (err) throw err
      chai.request(server)
      .get('/api/eleve/' + eleve.id)
      .send(eleve)
      .end((err, res) => {
         if (err) throw err
         res.should.have.status(200);
         res.body.should.be.a('object');
         res.body.should.have.property('sucess').eql(true);
         res.body.should.have.property('message').eql("Élève trouvé !");
         res.body.should.have.property('eleve');
         res.body.eleve.should.have.property('_id').eql(eleve.id);
         done();
      });
   });
   });
});

   /*
   * Test /GET/:nom Elève
   */
   describe('/GET/:nom Elève', () => {
      it('doit récupérer un élève par son nom', (done) => {
      let eleve = new Eleve({
         eleNom: "Doe",
         elePrenom: "John",
         eleClasse: "5f82c16d0a5a0a243726df24"
      });
      eleve.save((err, eleve) => {
         if (err) throw err
         chai.request(server)
         .get('/api/eleve/nom/' + eleve.eleNom)
         .send(eleve)
         .end((err, res) => {
            if (err) throw err
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('sucess').eql(true);
            res.body.should.have.property('message').eql("Elève trouvé !");
            res.body.should.have.property('eleve')
            res.body.eleve.should.be.a('array')
            res.body.eleve[0].should.have.property('eleNom').eql(eleve.eleNom)
            done();
         });
      });
      });
   });

   /*
   * Test /PUT/:id Elève
   */
   describe('/PUT/:id Elève', () => {
   it('doit UPDATE un élève par son id', (done) => {
      let eleve = new Eleve({
         eleNom: "Doe",
         elePrenom: "John",
         eleClasse: "5f82c16d0a5a0a243726df24"
      });
   eleve.save((err, eleve) => {
      if (err) throw err
      chai.request(server)
      .put('/api/eleve/' + eleve.id)
      .send({
         eleNom: "Dodo",
         elePrenom: "Jane",
         eleClasse: "5f82c16d0a5a0a243726df24"
      })
      .end((err, res) => {
         if (err) throw err
         res.should.have.status(200);
         res.body.should.be.a('object');
         res.body.should.have.property('message').eql('Les données de l\'élève ont été mise à jour.');
         res.body.eleve.should.have.property('eleNom').eql("Dodo");
         res.body.eleve.should.have.property('elePrenom').eql("Jane");
         done();
      });
   });
   });

   it('ne doit pas UPDATE un élève car erreur validation', (done) => {
      let eleve = new Eleve({
         eleNom: "Doe",
         elePrenom: "John",
         eleClasse: "5f82c16d0a5a0a243726df24"
      });
   eleve.save((err, eleve) => {
      if (err) throw err
      chai.request(server)
      .put('/api/eleve/' + eleve.id)
      .send({
         eleNom: "Dodo<script>",
         elePrenom: "Jane",
         eleClasse: "5f82c16d0a5a0a243726df24"
      })
      .end((err, res) => {
         if (err) throw err
         res.should.have.status(401);
         res.body.should.be.a('object');
         res.body.should.have.property('message').eql('Validation failed: eleNom: Dodo<script> est un nom invalide');
         res.body.should.have.property('sucess').eql(false)
         done();
      });
   });
   });
});

   /*
   * Test /DELETE/:id Elève
   */
   describe('/DELETE/:id Elève', () => {
      it('doit supprimer un élève par son id', (done) => {
         let eleve = new Eleve({
            eleNom: "Doe",
            elePrenom: "John",
            eleClasse: "5f82c16d0a5a0a243726df24"
         });
      eleve.save((err, eleve) => {
         chai.request(server)
         .delete('/api/eleve/' + eleve.id)
         .end((err, res) => {
            if (err) throw err
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql(`L'élève avec l'ID ${eleve.id} été supprimé`);
            res.body.should.have.property("eleve").eql({});
            res.body.should.have.property('sucess').eql(true);
            done();
         });
      });
      });
   });
});


//
// ─── MON BLOCK REUNIONS ──────────────────────────────────────────────────────
//
describe('Test API Réunion', () => {
   beforeEach((done) => { //Avant chaques test on vide la DB
      Reunion.deleteMany({}, (err) => {
         if (err) throw err
         done();
      });
   });

   /*
   * Test /GET ALL
   */
   describe('/GET Réunions', () => {
      it('doit récupérer toutes les réunions', (done) => {
         chai.request(server)
            .get('/api/reunions')
            .end((err, res) => {
                  if (err) throw err
                  res.should.have.status(200);
                  res.body.should.have.property('reunions').which.is.an('array')
                  res.body.should.have.property('total').to.equal(0)
                  res.body.should.have.property('sucess').eql(true);
            done();
            });
      });
   });

   /*
   * Test /PUT/:id Réunion
   */
   describe('/PUT/:id Réunion', () => {
      it('doit UPDATE une réunion par son id', (done) => {
         let reunion = new Reunion({
            title: "Conseil d'administration à 18h",
            date: "2020-12-01T00:00:00.000Z"
         });
      reunion.save((err, eleve) => {
         if (err) throw err
         chai.request(server)
         .put('/api/reunion/' + reunion.id)
         .send({
            title: "Journée Porte ouverte",
            date: "2020-12-02T00:00:00.000Z"
         })
         .end((err, res) => {
            if (err) throw err
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Les données de la réunion ont été mise à jour.');
            res.body.should.have.property('reunion')
            res.body.reunion.should.have.property('title').eql("Journée Porte ouverte");
            res.body.reunion.should.have.property('date').eql('2020-12-02T00:00:00.000Z')
            res.body.should.have.property('sucess').eql(true)
            done();
         });
      });
      });

      it('ne doit pas valider l\'UPDATE de la réunion', (done) => {
         let reunion = new Reunion({
            title: "Conseil d'administration à 18h",
            date: "2020-12-01T00:00:00.000Z"
         });
      reunion.save((err, eleve) => {
         if (err) throw err
         chai.request(server)
         .put('/api/reunion/' + reunion.id)
         .send({
            title: "Journée Porte ouverte<br>",
            date: "2020-12-02T00:00:00.000Z"
         })
         .end((err, res) => {
            if (err) throw err
            res.should.have.status(401);
            res.body.should.have.property("sucess").eql(false)
            res.body.should.have.property('message').eql('Validation failed: title: Journée Porte ouverte<br> est un titre invalide')
            done();
         });
      });
      });
   })

   /*
   * Test /POST Réunion
   */
   describe('/POST Réunion', () => {
      it('doit ajouter une réunion', (done) => {
         let reunion = {
            title: "Conseil de classe des 4ème B",
            date: "2020-01-01"
         }
         chai.request(server)
         .post('/api/reunion')
         .send(reunion)
         .end((err, res) => {
            if (err) throw err
            res.should.have.status(201);
            res.body.should.be.a('object');
            res.body.should.have.property('reunion')
            res.body.should.have.property('sucess').eql(true);
            res.body.should.have.property('message').eql("Réunion ajoutée avec succès !")
            done();
         });
      });

   it('ne doit pas ajouter une réunion car champs manquant', (done) => {
      let reunion = {
         title: "Conseil de classe des 6èmeC"
      }
      chai.request(server)
      .post('/api/reunion')
      .send(reunion)
      .end((err, res) => {
         if (err) throw err
         res.should.have.status(401);
         res.body.should.be.a('object');
         res.body.should.have.property('errors')
         res.body.errors.should.have.property('date');
         res.body.errors.date.should.have.property('kind').eql("required")
         done();
      });
   });

   it('ne doit pas ajouter une reunion car erreur validation', (done) => {
      let reunion = {
         title: "Conseil d'administration<h1>bonjour</h1>",
         date: "2020-11-15"
      }
      chai.request(server)
      .post('/api/reunion')
      .send(reunion)
      .end((err, res) => {
         if (err) throw err
         res.should.have.status(401);
         res.body.should.be.a('object');
         res.body.should.have.property('errors')
         res.body.errors.should.have.property('title');
         res.body.errors.title.should.have.property('message').eql("Conseil d'administration<h1>bonjour</h1> est un titre invalide")
         done();
      });
   });
   });

   /*
   * Test /DELETE/:id Réunion
   */
   describe('/DELETE/:id Réunion', () => {
      it('doit supprimer une réunion par son id', (done) => {
         let reunion = new Reunion({
            title: 'Ajout de la nouvelle réunion',
            date: "2020-01-01"
         });
         reunion.save((err, reunion) => {
            if (err) throw err
            chai.request(server)
            .delete('/api/reunion/' + reunion.id)
            .end((err, res) => {
               if (err) throw err
               res.should.have.status(200);
               res.body.should.be.a('object');
               res.body.should.have.property('message').eql(`La réunion avec l'ID ${reunion.id} été supprimée`);
               res.body.should.have.property("reunion").eql({});
               res.body.should.have.property('sucess').eql(true);
               done();
            });
         });
      });
   });

      /*
   * Test /GET/:id Réunion
   */
   describe('/GET/:id Réunion', () => {
      it('doit récupérer une réunion par son id', (done) => {
      let reunion = new Reunion({
         title: 'Nouvelle réunion de test',
         date: "2020-12-25"
      });
      reunion.save((err, reunion) => {
         if (err) throw err
         chai.request(server)
         .get('/api/reunion/' + reunion.id)
         .send(reunion)
         .end((err, res) => {
            if (err) throw err
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('sucess').eql(true);
            res.body.should.have.property('message').eql("Réunion trouvé !");
            res.body.should.have.property('reunion');
            res.body.reunion.should.have.property('_id').eql(reunion.id);
            done();
         });
      });
      });
   });

});