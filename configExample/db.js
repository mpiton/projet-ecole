const mongoose = require('mongoose');

const connectDb = async () => {
   const connexion = await mongoose.connect(process.env.MONGO_URI, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true
   });

   console.log(`Mongodb est connecté à l'hôte: ${connexion.connection.host}`.green.bold)
}

module.exports = connectDb;