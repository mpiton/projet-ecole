// Dépendances
const faker = require("faker");
const assert = require("assert");
const MongoClient = require("mongodb").MongoClient;
const dotenv = require('dotenv');

//Ajout valeurs d'environnement
dotenv.config({path: "../config.env"});
const url = process.env.MONGO_URI; //Placer votre mongo URI ici

// nom de la base
const dbName = "ecole";

// Franciser les données
faker.locale = "fr";

// Connexion au server mongo
MongoClient.connect(url,
   {
   useNewUrlParser: true,
   useUnifiedTopology: true
   },
   function(err, client) {
   assert.strictEqual(null, err);
const db = client.db(dbName);

  // Accès aux collections
  const ProfsCollection = db.collection("profs");
  const ElevesCollection = db.collection("eleves");
  // Création des profs
  let profs = [];
  for (let i = 0; i < 20; i += 1) {
    const firstName = faker.name.firstName();
    const lastName = faker.name.lastName();
    let newProf = {
      profMail: faker.internet.email(firstName, lastName),
      profNom: lastName,
      profPrenom: firstName,
      profTel: faker.phone.phoneNumber(),
      profPhoto: faker.image.avatar(),
      dateArrive: faker.date.recent(),
      adresse: {
         codePostal: faker.address.zipCode(),
         rue: faker.address.streetAddress(),
         ville: faker.address.city()
      },
      profClasses: "5f97ccf0ef8f7e0532758f17",
      profMatiere: "5f97ccd1ef8f7e0532758f15"
    };
    profs.push(newProf);

    // feedback visuel
    console.log(newProf.profMail);
  }
  ProfsCollection.insertMany(profs);

   // Création des Eleves
   let eleves = [];
   for (let i = 0; i < 20; i += 1) {

     let newEleve = {
      eleNom: faker.name.lastName(),
      elePrenom: faker.name.firstName(),
      eleTel: faker.phone.phoneNumber(),
      eleTelSec: faker.phone.phoneNumber(),
      eleBirthday: faker.date.recent(),
      eleAdresse: {
         codePostal: faker.address.zipCode(),
         rue: faker.address.streetAddress(),
         ville: faker.address.city()
      },
      eleComment: faker.lorem.sentence(),
      eleClasse: "5f97ccf0ef8f7e0532758f17"
     };
     eleves.push(newEleve);
 
     // feedback visuel
     console.log(newEleve.eleNom);
   }
   ElevesCollection.insertMany(eleves);
  console.log("Database seeded! :)");
  client.close()
});
