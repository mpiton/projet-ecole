<span style="display:block;text-align:center">![logo du site](./client/src/images/logo.png)</span>
# Projet Ecole

<span style="display:block;text-align:center;width: 100%">![présentation en image](http://www.mathieu-piton.com/img/projet-ecole.png)</span>

Démo: https://ecole.4o4.fr/

Pour configurer votre base de donnée, vous devez:
- Renommer le dossier "configExample" en "config"
- Modifier le "config.env" en ajoutant votre port de connexion et l'URL de votre base MongoDB

Pour remplir votre base de donnée faite: (Attention vous allez remplir 5000 entrées dans la base)
```bash
node config/seed/seed.js
```

## Par défaut:
Port client: 3000
Port Server: 5000

## Installer les dépendances:
```bash
npm install
cd client
npm install
```

###  Pour lancer le Front + le Back:
```bash
npm run dev
```
### Pour lancer le Front seulement:
```bash
cd client
npm start
```

### Pour lancer le Back seulement:
```bash
npm run server
```
#### Login et mot de passe de connexion au panel admin:
````bash
Login: ifpa@dev.io
Mot de passe: ifpa2020
````

### Chemin d'accès vers la documentation de l'api: `http://localhost:PORT/api-docs/`


## Besoin fonctionnel

### Gestion professeurs
- Liste des professeurs -> Fiches professeurs
- CRUD professeurs
- Planning des professeurs

### Gestion Elèves
- Liste des élèves -> Fiches élèves
- Bulletins de notes (CRUD)
- CRUD élèves
- Emploi du temps des classes

## Besoin non fonctionnel
- Affichage des statistiques sur le tableau de bord
- Création d'un tchat interne

### Gestion Agents (optionnel)
- Liste des agents
- Stocks de nourriture
- Stock de produit d'entretien
- Gestion des menus de la cantine
- CRUD Agents

## Langages utilisés

Skills : Stack MERN

## TODO

### Backend
- [x] Création du projet Node
- [x] Création de la DB
- [x] Connexion à la DB
- [x] Création du modèle Élève
- [x] Création du modèle Professeur
- [x] Création du Router
- [x] Test d'ajout d'un élève dans la DB en POST (avec POSTMAN)
- [x] Récupération des élèves en GET
- [x] Mise à jour d'un élève avec PUT
- [x] Suppression d'un élève avec DELETE
- [x] Ajout méthode rechercher profs par nom
- [x] Ajout méthode rechercher élèves par nom
- [x] Création du Schema Reunion
- [x] Ajout méthode POST et GET Reunion
- [x] Création Schema des classe des élèves
- [x] Création des méthode POST et GET des classes
- [x] Sécurisation du backend

### Frontend
- [x] Construction des routes sous React
- [x] Création du dashboard admin
- [x] Liste professeurs en static
- [x] Pagination des listes
- [x] Liste professeurs hooked et dynamique
- [x] Création component Réunion professeurs en static
- [x] Réfléchir à la la création du component Réunion
- [x] Liste Eleves en static
- [x] Liste Eleves en dynamique
- [ ] Réfléchir à la gestion des Bulletins de notes
- [x] Création en static du Bulletins de notes
- [x] Création en static de l'emploi du temps
- [ ] réfléchir à l'intéraction avec ce component
- [x] réfléchir à la fonction déconnexion
- [x] Ecrire la fonction déconnexion
- [ ] Créer un tableau de bord statique
- [x] Faire la fonction de recherche par nom prof et élèves
- [ ] Voir comment gérer l'erreur si l'utilisateur recherché n'existe pas
- [x] Update infos professeurs dans liste
- [ ] Edit profs et élève en modal
- [x] Custom les messages de chargement de components
- [ ] Création de login et mot de passe lors de la création de professeurs
- [x] Hook data Réunions
- [x] Création des méthodes des réunions
- [x] Sécurisation des champs
- [x] Formulaire de création de classe
- [x] Fonction ajout, suppression, édition des classes
- [x] Degobage select formulaire d'ajout de professeurs
- [x] Réorganisation du menu
- [ ] Mettre les boutons ajouter hors du menu
- [ ] Création carte pour avoir les infos en détail de l'élève
- [ ] Création carte pour avoir les infos en détail du professeur
- [ ] Responsive à faire


### Options

- [ ] dynamiser le tableau de bord avec les données
- [ ] création d'un tchat pour les professeurs
- [ ] Liste des agents
- [ ] Stocks de nourriture
- [ ] Stock de produit d'entretien
- [ ] Gestion des menus de la cantine
- [ ] CRUD Agents
- [ ] Déploiement CI/CD sur Gitlab
- [ ] Envoie automatique d'email à la création du compte utilisateur
- [x] Débogage du seeder niveau eleClasse
- [x] Création de la documentation Swagger
- [x] Adapter les routes à la convention REST API
- [x] Test d'API avec Mocha et Chai




## Ressource utiles
https://www.fusioncharts.com/react-charts?framework=react (Diagramme Tableau de bord)